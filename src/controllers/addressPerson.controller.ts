/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Person from '../models/person';
import Users from '../models/dgUsers';
import AddressPerson from '../models/addressPerson';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {

  try {
    const data = await Users.findOne({
      where: { usr_id: request.params.id, usr_mode: 1 },
      include: [
        {
          model: Person,
          attributes: ['prs_id', 'prs_document', 'prs_name', 'prs_lastname']
        },
      ],
    });
    // --------------------------------------------------------
    if (!data) {
      return response.status(200).send({ status: 'success', message: `¡Usuario no encontrado!` });
    }

    const NEW_CONFIG = {
      where: { adp_person_id: data['dataValues']['person'].prs_id },
    };
    const result = await AddressPerson.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get all 
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await AddressPerson.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create 
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response, usr_id: number, role_id: number) {
  const payload = request.body;
  try {
    const data = await Users.findOne({
      where: { usr_id: payload.usr_id, usr_mode: 1 },
      include: [
        {
          model: Person,
          attributes: ['prs_id', 'prs_document', 'prs_name', 'prs_lastname']
        },
      ],
    });
    if (!data) {
      return response.status(200).send({ status: 'success', message: `¡Usuario no encontrado!` });
    }
    const result = await AddressPerson.create({
      ...payload,
      adp_person_id: data['dataValues']['person'].prs_id,
      adp_created_at: new Date(),
      adp_creator_id: usr_id
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update 
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response, usr_id: number, role_id: number) {
  const payload = request.body;
  try {
    const result = await AddressPerson.findByPk(request.params.id);
    const data = await result.update({
      ...payload,
      adp_updated_at: new Date(),
      adp_editor_id: usr_id
    });
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete 
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await AddressPerson.findByPk(request.params.id);
    const data = await result.destroy();
    response.status(200).send({ message: 'success' });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
