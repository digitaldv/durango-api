/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import CancelType from '../models/cancelType';

/************ Functions ************/
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {

  const params: any = request.query;
  const limit = { limit: Number(params.rows) || 20 };
  const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
  const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
  // ---------------------------------------------------------------------------------
  // delete foreign key
  const exclude_keys = [
    { key_id: 'cnlt_id' }
  ];
  const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
  // ---------------------------------------------------------------------------------
  const cnltId = !isEmpty(params) ? JSON.parse(params.filters).cnlt_id || '' : {};


  const NEW_CONFIG = !isEmpty(params) ? {
    ... offset,
    ... limit,
    ... order,
    where: cnltId === '' ? where : { ...where, cnlt_id: cnltId },
  }: {
    where,
    ... order,
  };

  try {
    const result = await CancelType.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await CancelType.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response, user_id) {
  const payload = request.body;
  try {
    const result = await CancelType.create({...payload, cnlt_creator_id: user_id, cnlt_editor_id: user_id, cnlt_created_at: new Date()});
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response, user_id) {
  const payload = request.body;
  try {
    const result = await CancelType.findByPk(request.params.id);
    const data = await result.update({...payload, cnlt_editor_id: user_id, cnlt_updated_at: new Date() });
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await CancelType.findByPk(request.params.id);
    if (result) {
      const data = await result.destroy();
      response.status(200).send({ message: 'success' });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
