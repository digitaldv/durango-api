/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Countries from '../models/countries';
import States from '../models/states';
import Cities from '../models/cities';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {

  const params: any = request.query;
  const limit = { limit: Number(params.rows) || 20 };
  const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
  const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
  // ---------------------------------------------------------------------------------
  // delete foreign key
  const exclude_keys = [
    { key_id: 'cty_id' },
    { key_id: 'states' },
  ];
  const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
  // ---------------------------------------------------------------------------------
  const statesCountry = !isEmpty(params) ? JSON.parse(params.filters).states || '' : {};
  const ctyId = !isEmpty(params) ? JSON.parse(params.filters).cty_id || '' : {};

  const NEW_CONFIG = !isEmpty(params) ? {
    ...offset,
    ...limit,
    ...order,
    where: ctyId === '' ?  where : { ... where, cty_id: ctyId },
    include: [
      {
        model: States, as: 'states',
        where: { dpr_name: { [OP.iLike]: `%${statesCountry}%` } },
      },
    ],
  } : {
    where,
    include: [
      {
        model: States, as: 'states',
        include: [
          {
            model: Countries,
          },
        ],
      },
    ],

  };

  try {
    const result = await Cities.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await Cities.findByPk(request.params.id, {
      include: [
        { model: States, attributes: ['dpr_id', 'dpr_name'] },
      ],
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Cities.create({ ...payload, cty_created_at: new Date() });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Cities.findByPk(request.params.id);
    const data = await result.update(payload);
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await Cities.findByPk(request.params.id);
    if (result) {
      const data = await result.destroy();
      response.status(200).send({ message: 'success' });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getStateById(request: Request, response: Response) {
  try {
    const result = await Cities.findAll({ 
      where : { cty_dpr_id: request.params.id},
      include: [
        { model: States, attributes: ['dpr_id', 'dpr_name'] },
      ],
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/** 
* @author Randall Medina
* @description Function to get cities by id
* @param request
* @param response
*/
export async function getCitiesFull(request: Request, response: Response) {
  try {
    const city = await sequelizeQuery.query(
      `SELECT
        c.cty_id "id", concat(d.dpr_name,' - ',c.cty_name) as "nameFull"
        FROM dba_new_db.cities c INNER JOIN dba_new_db.states d 
        ON c.cty_dpr_id = d.dpr_id
        where concat( d.dpr_name ,' ',unaccent(c.cty_name),' ',c.cty_id) ilike ('%' || unaccent(:search) || '%') LIMIT :limit;`
      ,
      {
        replacements: {
          search: `%${request.params.search}%`,
          limit: request.params.limit || 100,
        }, type: sequelizeQuery['QueryTypes'].SELECT,
      },
    );
    response.status(200).send(city);

  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
