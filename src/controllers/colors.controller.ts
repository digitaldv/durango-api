/************ import libraries ************/
import { isEmpty } from "lodash";
import Sequelize = require("sequelize");
import { Request, Response } from "express";

/************ import models ************/
import Colors from "../models/colors";
/************ Functions ************/
import { orderModel } from "../functions/orderModel";
import { whereModel } from "../functions/whereModel";

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  const params: any = request.query;
  const limit = { limit: Number(params.rows) || 20 };
  const offset = {
    offset: (Number(params.page) || 0) * (Number(params.rows) || 20),
  };
  const order = !isEmpty(params.filters)
    ? orderModel(params.order_field, params.order_type)
    : {};
  // ---------------------------------------------------------------------------------
  // delete foreign key
  const exclude_keys = [{ key_id: "co_id" }];
  const where = await whereModel(
    !isEmpty(params.filters) ? JSON.parse(params.filters) : {},
    exclude_keys
  );
  // ---------------------------------------------------------------------------------
  const coId = !isEmpty(params) ? JSON.parse(params.filters).co_id || "" : {};

  const orderNot = orderModel("co_name", "ASC");
  const NEW_CONFIG = !isEmpty(params)
    ? {
        ...offset,
        ...limit,
        where: coId === '' ? {...where, co_erased: false} : { ...where, co_id: coId, co_erased: false },
        ...order,
      }
    : {
        where: { ...where, co_erased: false, co_state: 1 },
        ...orderNot,
      };

  try {
    const result = await Colors.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await Colors.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Colors.create({ ...payload, co_created_at: new Date() });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Colors.findByPk(request.params.id);
    const data = await result.update({ ...payload, co_updated_at: new Date() });
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await Colors.findByPk(request.params.id);
    if (result) {
      const data = await result.update({ co_erased: true });
      // const data = await result.destroy();
      response.status(200).send({ message: "success" });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
