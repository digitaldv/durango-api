/************ import libraries ************/
import { isEmpty } from 'lodash';
import { Request, Response } from 'express';

/************ import models ************/
import DocumentType from '../models/documentType';

/************ Functions ************/
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/**
 * @author Randall Medina
 * @description Function to get all 
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  try {

    const params: any = request.query;
    const limit = { limit: Number(params.rows) || 20 };
    const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
    const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
    // ---------------------------------------------------------------------------------
    // delete foreign key
    const exclude_keys = [
      { key_id: 'dt_id' }
    ];
    const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
    // ---------------------------------------------------------------------------------
    const dtId = !isEmpty(params) ? JSON.parse(params.filters).dt_id || '' : {};

    const NEW_CONFIG = !isEmpty(params) ? {
      ...offset,
      ...limit,
      ...order,
      where: dtId === '' ? where : { ...where, dt_id: dtId },
    } : {
      where,
    };

    const result = await DocumentType.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get  by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await DocumentType.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create 
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  try {
    const result = await DocumentType.create({ ...payload, dt_created_at: new Date(), dt_creator_id: usr_id });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update 
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response, user_id: number) {
  const payload = request.body;
  try {
    const result = await DocumentType.findByPk(request.params.id);
    const data = await result.update({ ...payload, dt_updated_at: new Date(), dt_editor_id: user_id });
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete 
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await DocumentType.findByPk(request.params.id);
    const data = await result.update({ dt_state: 0 });
    response.status(200).send({ message: 'success' });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
