/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;
