
import to from 'await-to-js';
import { isEmpty, isNil } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';

/************ import models ************/
import Cities from '../models/cities';
import DocumentType from '../models/documentType';
import Users from '../models/dgUsers';
import Employee from '../models/employee';
import Roles from '../models/dgRoles';
import UserRoles from '../models/userRoles';
import States from '../models/states';

/************ Functions ************/
// import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';
import { orderModel } from '../functions/orderModel';
import { getHash, compareHash } from '../functions/Hash';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  try {

    const params: any = request.query;
    const limit = { limit: Number(params.rows) || 20 };
    const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
    const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
    // ---------------------------------------------------------------------------------
    // delete foreign key
    const exclude_keys = [
      { key_id: 'emp_id' },
      { key_id: 'emp_cty_id' },
      { key_id: 'emp_dct_id' },
      { key_id: 'emp_rle_id' },
    ];
    const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
    // ---------------------------------------------------------------------------------
    const empId = !isEmpty(params) ? JSON.parse(params.filters).emp_id || '' : {};
    const empCtyId = !isEmpty(params) ? JSON.parse(params.filters).emp_cty_id || '' : {};
    const empDctId = !isEmpty(params) ? JSON.parse(params.filters).emp_dct_id || '' : {};
    const empRleId = !isEmpty(params) ? JSON.parse(params.filters).emp_rle_id || '' : {};

    const NEW_CONFIG = !isEmpty(params) ? {
      ...offset,
      ...limit,
      ...order,
      where: empId === '' ? where : { ...where, emp_id: empId },
      include: [
        {
          model: Cities, as: 'cities',
          where: { cty_name: { [OP.iLike] : `%${empCtyId}%` } },
        },
        {
          model: DocumentType, as: 'documentType',
          attributes: { exclude: ['dt_created_at', 'dt_updated_at'] },
          where: { dt_description: { [OP.iLike] : `%${empDctId}%` } },
        },
        {
          model: Users, as: 'user',
          attributes: ['usr_id', 'usr_username', 'usr_email'],
          include: [
            {
              model: UserRoles,
              attributes: ['usrl_id', 'usrl_rle_id', 'usrl_usr_id'],
              include: [
                {
                  model: Roles,
                  where: { rle_name: { [OP.iLike] : `%${empRleId}%` } },
                },
              ]
            },
          ]
        },
      ],
    } : {
      where,
      include: [
        {
          model: Cities, as: 'cities',
        },
        {
          model: DocumentType, as: 'documentType',
          attributes: { exclude: ['dt_created_at', 'dt_updated_at'] },
        },
        {
          model: Users, as: 'user',
          attributes: ['usr_id', 'usr_mode', 'usr_username'],
        },
      ],

    };

    const employee = await Employee.findAndCountAll(NEW_CONFIG);
    response.status(200).send(employee);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const employee = await Employee.findByPk(request.params.id, {
      attributes: { exclude: ['emp_created_at', 'emp_creator_id', 'emp_editor_id', 'emp_updated_at'] },
      include: [
        {
          model: Cities, as: 'cities',
          include: [
            {
              model: States,
            },
          ]
        },
        {
          model: DocumentType, as: 'documentType',
          attributes: { exclude: ['dt_created_at', 'dt_updated_at'] },
        },
        {
          model: Users, as: 'user',
          attributes: ['usr_id', 'usr_username', 'usr_email'],
          include: [
            {
              model: UserRoles,
              attributes: ['usrl_id', 'usrl_rle_id', 'usrl_usr_id'],
              include: [
                {
                  model: Roles,
                },
              ]
            },
          ]
        },
      ],
    });
    response.status(200).send(employee);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  try {
    // Se verifica si existe el email
    const resultEmail = await Users.findOne({ where: { usr_email: payload.emp_email, usr_erased: false } });
    if (resultEmail) {
      return response.status(201).send({ status: 'info', message: '¡El email que ha ingresado YA existe! Verifique e intente de nuevo.', exists: true });
    }
    // ------------------------------------------------------------
    // contraseña alfanumerica de 6 digitos
    // const accessCode = await Array.from({ length: 1 }, () => Math.random().toString(36).substr(2, 6));
    // const passEncrypt = await getHash(`${accessCode[0].toUpperCase()}`);
    const accessCode = '1234567a';
    const passEncrypt = await getHash(accessCode);
    // ------------------------------------------------------------
    const [errUsr, resultUsr] = await to(Users.create({
      usr_username: `${payload.emp_name} ${payload.emp_lastname}`,
      usr_password: passEncrypt,
      usr_email: payload.emp_email,
      usr_phone_number: payload.emp_cellphone,
      usr_creator_id: usr_id,
      usr_created_at: new Date(),
      usr_origin: 'WEB',
      usr_erased: false,
    }));
    if (errUsr) {
      return response.status(500).send({ status: 'error', message: errUsr.message });
    }
    // ------------------------------------------------------------
    const [errRol, resultRol] = await to(UserRoles.create({
      usrl_rle_id: payload.emp_rle_id,
      usrl_usr_id: resultUsr.getDataValue('usr_id'),
      usrl_created_at: new Date(),
    }));
    if (errRol) {
      await resultUsr.destroy()
      return response.status(500).send({ status: 'error', message: errRol.message });
    }
    // ------------------------------------------------------------
    const [errColaborador, resultColaborador] = await to(Employee.create({
      ...payload,
      emp_erased: false,
      emp_usr_id: resultUsr.getDataValue('usr_id'),
      emp_created_at: new Date(),
      emp_creator_id: usr_id
    }));
    if (errColaborador) {
      await resultRol.destroy()
      await resultUsr.destroy()
      return response.status(500).send({ status: 'error', message: errColaborador.message });
    }
    // ------------------------------------------------------------
    response.status(200).send({ status: 'success', message: '¡Registro creado con exito!' });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  try {
    const employee = await Employee.findByPk(request.params.id);
    // ------------------------------------------------------------
    if (payload.emp_email !== employee.getDataValue('emp_email')) {
      // ------------------------------------------
      const existEmail = await Employee.findAll({ where: { emp_email: payload.emp_email, emp_usr_id: { [OP.ne]: employee.getDataValue('emp_usr_id') } } });
      const existUserEmail = await Users.findAll({ where: { usr_email: payload.emp_email, usr_id: { [OP.ne]: employee.getDataValue('emp_usr_id') } } });
      if (!isEmpty(existEmail) || !isEmpty(existUserEmail)) {
        return response.status(201).send({ status: 'info', message: 'Error: El correo ingresado ya pertenece a otro usuario. Verifique e intente de nuevo.' });
      }
      // ------------------------------------------
      const data = await employee.update({
        ...payload,
        emp_editor_id: usr_id,
        emp_updated_at: new Date(),
      });
      const resultUser = await Users.findByPk(employee.getDataValue('emp_usr_id'));
      await resultUser.update({
        usr_email: payload.emp_email,
        usr_username: `${payload.emp_name} ${payload.emp_lastname}`,
        usr_phone: payload.emp_cellphone !== employee.getDataValue('emp_cellphone') ? payload.emp_cellphone : employee.getDataValue('emp_cellphone'),
        usr_editor_id: usr_id,
        usr_updated_at: new Date(),
      });
      response.status(200).send(data);
      // ------------------------------------------
    } else {
      // ------------------------------------------
      const data = await employee.update({
        ...payload, emp_updated_at: new Date(), emp_editor_id: usr_id
      });
      const resultUser = await Users.findByPk(employee.getDataValue('emp_usr_id'));
      await resultUser.update({
        usr_phone: payload.emp_cellphone !== employee.getDataValue('emp_cellphone') ? payload.emp_cellphone : employee.getDataValue('emp_cellphone'),
        usr_editor_id: usr_id,
        usr_updated_at: new Date(),
      });
      // ------------------------------------------
      response.status(200).send(data);
    }
    // ------------------------------------------------------------
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const employee = await Employee.findByPk(request.params.id);
    const prs = await employee.destroy();
    response.status(200).send(prs);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}