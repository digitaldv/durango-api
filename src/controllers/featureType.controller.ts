/************ import libraries ************/
import { isEmpty } from "lodash";
import { Request, Response } from "express";

/************ import models ************/
import FeatureType from "../models/featureType";

/************ Functions ************/
import { orderModel } from "../functions/orderModel";
import { whereModel } from "../functions/whereModel";

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  try {
    const params: any = request.query;
    const limit = { limit: Number(params.rows) || 20 };
    const offset = {
      offset: (Number(params.page) || 0) * (Number(params.rows) || 20),
    };
    const order = !isEmpty(params.filters)
      ? orderModel(params.order_field, params.order_type)
      : {};
    // ---------------------------------------------------------------------------------
    // delete foreign key
    const exclude_keys = [{ key_id: "ftt_id" }];
    const where = await whereModel(
      !isEmpty(params.filters) ? JSON.parse(params.filters) : {},
      exclude_keys
    );
    // ---------------------------------------------------------------------------------
    const fttId = !isEmpty(params)
      ? JSON.parse(params.filters).ftt_id || ""
      : {};

    const NEW_CONFIG = !isEmpty(params)
      ? {
          ...offset,
          ...limit,
          ...order,
          where: fttId === '' ? where : { ...where, ftt_id: fttId },
        }
      : {
          where,
        };

    const result = await FeatureType.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get  by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await FeatureType.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(
  request: Request,
  response: Response,
  user_id: number
) {
  const payload = request.body;
  try {
    const result = await FeatureType.create({
      ...payload,
      ftt_created_at: new Date(),
      ftt_creator_id: user_id,
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response, user_id) {
  const payload = request.body;
  try {
    const result = await FeatureType.findByPk(request.params.id);
    const data = await result.update({
      ...payload,
      ftt_updated_at: new Date(),
      ftt_editor_id: user_id,
    });
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await FeatureType.findByPk(request.params.id);
    const data = await result.update({ ftt_state: 0 });
    response.status(200).send({ message: "success" });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
