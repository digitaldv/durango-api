/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Passenger from '../models/passenger';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getByUserId(request: Request, response: Response) {
    try {
        const result = await Passenger.findOne({ where: { psg_usr_id: request.params.id } });
        response.status(200).send(result);
    } catch (error) {
        response.status(500).send({ message: error.message });
    }
}