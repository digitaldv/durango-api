/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Patterns from '../models/patterns';
import Marks from '../models/marks';

/************ Functions ************/
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {

  const params: any = request.query;
  const limit = { limit: Number(params.rows) || 20 };
  const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
  const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
  // ---------------------------------------------------------------------------------
  // delete foreign key
  const exclude_keys = [
    { key_id: 'pat_id' },
    { key_id: 'pat_mk_id' },
  ];
  const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
  // ---------------------------------------------------------------------------------
  const patId = !isEmpty(params) ? JSON.parse(params.filters).pat_id || '' : {};
  const marca = !isEmpty(params) ? JSON.parse(params.filters).pat_mk_id || '' : {};

  const NEW_CONFIG = !isEmpty(params) ? {
    ... offset,
    ... limit,
    ... order,
    where: patId === '' ? where : { ...where, pat_id: patId },
    include: [
      {
        model: Marks, as: 'mark',
        attributes: { exclude: ['mk_created_at', 'mk_updated_at'] },
        where: { mk_name: { [OP.iLike] : `%${marca}%` } },
      },
    ],
  } : {
    where : { ...where, pat_state: 1 },
    include: [
      {
        model: Marks, as: 'mark',
      },
    ],
     
  };

  try {
    const result = await Patterns.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await Patterns.findByPk(request.params.id, {
      include: [
        { model: Marks , attributes :['mk_id', 'mk_name'] },
      ],
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Patterns.create({...payload, pat_created_at: new Date()});
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Patterns.findByPk(request.params.id);
    const data = await result.update({...payload, pat_updated_at: new Date() });
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await Patterns.findByPk(request.params.id);
    if (result) {
      const data = await result.destroy();
      response.status(200).send({ message: 'success' });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}