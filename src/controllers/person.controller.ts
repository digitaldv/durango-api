/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Person from '../models/person';
import Users from '../models/dgUsers';
import Cities from '../models/cities';
import DocumentType from '../models/documentType';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
    try {
        const data = await Users.findOne({
            where: { usr_id: request.params.id },
        });
        if (!data) {
            return response.status(200).send({ status: 'success', message: `¡Usuario no encontrado!` });
        }
        const person = await Person.findOne({
            where: { prs_usr_id: data.getDataValue('usr_id') },
            attributes: { exclude: ['prs_created_at', 'prs_creator_id', 'prs_editor_id', 'prs_updated_at', 'prs_count'] },
            include: [
                {
                    model: Users, as: 'user',
                    attributes: ['usr_id', 'usr_mode', 'usr_username', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
                },
                { model: Cities, attributes: ['cty_id', 'cty_name'] },
                { model: DocumentType, attributes: ['dt_id', 'dt_code', 'dt_description'] },
            ],
        });
        response.status(200).send(person);
    } catch (error) {
        response.status(500).send({ message: error.message });
    }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
    const payload = request.body;
    try {
        const data = await Users.findOne({
            where: { usr_id: request.params.id },
        });
        if (!data) {
            return response.status(200).send({ status: 'success', message: `¡Usuario no encontrado!` });
        }

        const person = await Person.findOne({ where: { prs_usr_id: data.getDataValue('usr_id') }, });
        const prs = await person.update(payload);
        response.status(200).send(prs);
    } catch (error) {
        response.status(500).send({ message: error.message });
    }
}
