/************ import libraries ************/
import { isEmpty } from 'lodash';
import to from 'await-to-js';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Users from '../models/dgUsers';
import Pines from '../models/pines';
import Passenger from '../models/passenger';
import Driver from '../models/driver';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function validatePine(request: Request, response: Response, usr_id: number, role_id: number) {
  const payload = request.body;
  try {
    const data = await Users.findOne({
      where: { usr_id: payload.usr_id },
    });
    if (!data) {
      return response.status(200).send({ status: 'success', message: `¡Usuario no encontrado!` });
    }

    const result_p = await Pines.findOne({ where: { pn_cod_pin: payload.pn_cod_pin, pn_state: 1, pn_driver_id: null } });
    if (result_p) {

      await result_p.update({ pn_driver_id: payload.usr_id, pn_state: 2, pn_updated_at: new Date() });
      // ------------------------------------
      // Cargar los viajes o descuentos a usuario
      // -----------------------------------
      // 2=Conductor
      // cargar los viajes
      const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
      dataDriver.update({ dvr_discount: dataDriver.getDataValue('dvr_discount') + Number(result_p.getDataValue('pn_number_services')) });
      // ------------------------------------
      const result = {
        recharge_value: result_p.getDataValue('pn_value'),
        recharge_pin: result_p.getDataValue('pn_cod_pin'),
        count_service: Number(result_p.getDataValue('pn_number_services')) // pn_number_services equivale a la cantidad de servicios/descuentos del valor del pin
      }
      response.status(200).send({ message: '¡Codigo valido!', result, validate: true, });
    } else {
      response.status(200).send({ message: '¡Codigo invalido!', validate: false });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create services
 * @param request
 * @param response
 */
export async function getAllPine(request: Request, response: Response, usr_id: number) {
  try {
    const order = orderModel('pn_updated_at', 'DESC');
    const data = await Users.findByPk(request.params.id);
    // const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
    const NEW_CONFIG = {
      where: { pn_driver_id: data.getDataValue('usr_id'), pn_state: 2 },
      ...order,
      include: [
        {
          model: Users, as : 'driver',
          attributes: ['usr_id', 'usr_mode', 'usr_username', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
        }
      ],
    };
    const result = await Pines.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}