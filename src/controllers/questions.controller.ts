/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Questions from '../models/questions';
import ScoreDriver from '../models/scoreDriver';
import ScorePassenger from '../models/scorePassenger';
import Passenger from '../models/passenger';
import Driver from '../models/driver';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
    try {
        //  Para Questions Passenger (id:1)
        //  Para Questions Driver (id:2)
        const result = await Questions.findByPk(request.params.id);
        response.status(200).send(result);
    } catch (error) {
        response.status(500).send({ message: error.message });
    }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function createAnswers(request: Request, response: Response) {
    const payload = request.body;
    try {
        if (payload.type === 'pasajero') {
            const result = await ScorePassenger.create({
                ...payload,
                scp_created_at: new Date()
            });
            response.status(200).send(result);
        } else {
            const result = await ScoreDriver.create({
                ...payload,
                scd_created_at: new Date()
            });
            response.status(200).send(result);
        }
    } catch (error) {
        response.status(500).send({ message: error.message });
    }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getAnswersById(request: Request, response: Response) {
    const params: any = request.query;
    try {
        if (params.type === 'pasajero') {
            const result = await ScorePassenger.findOne({
                where: { scp_id: request.params.id },
                include: [
                    {
                        model: Questions
                    },
                    {
                        model: Passenger
                    }
                ],
            }
            );
            response.status(200).send(result);
        } else {
            const result = await ScoreDriver.findOne({
                where: { scd_id: request.params.id },
                include: [
                    {
                        model: Questions
                    },
                    {
                        model: Driver
                    }
                ],
            }
            );
            response.status(200).send(result);
        }
    } catch (error) {
        response.status(500).send({ message: error.message });
    }
}
