/************ import libraries ************/
import axios from 'axios';
import to from 'await-to-js';
import sequelize from '../db-config';
import Sequelize = require('sequelize');
import { isEmpty } from 'lodash';
import { Request, Response } from 'express';

/** import models */

import Person from '../models/person';
import FeatureVehicle from '../models/featureVehicle';
import Patterns from '../models/patterns';
import Colors from '../models/colors';
import RequestDriver from '../models/requestDriver';
import Users from '../models/dgUsers';
import FeatureDriver from '../models/featureDriver';
import Driver from '../models/driver';
import MotiveFeatureDriver from '../models/motiveFeatureDriver';
import CarModels from '../models/carModels';
import DriverState from '../models/driverState';
import DriverStatus from '../models/driverStatus';
import FeatureType from '../models/featureType';
import Cities from '../models/cities';
import DocumentType from '../models/documentType';
import States from '../models/states';
import Marks from '../models/marks';

/************ Functions ************/
import { sendSMS, pushNotifications } from '../functions/sendMessage';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';
/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  try {
    const params: any = request.query;
    const limit = { limit: Number(params.rows) || 20 };
    const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
    const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
    // ---------------------------------------------------------------------------------
    // delete foreign key
    const exclude_keys = [
      { key_id: 'rqd_id' },
      { key_id: 'rqd_person_id' },
      { key_id: 'rqd_user_id' },
    ];
    const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
     // ---------------------------------------------------------------------------------
    const rqdId = !isEmpty(params) ? JSON.parse(params.filters).rqd_id || '' : {};
    const person = !isEmpty(params) ? JSON.parse(params.filters).rqd_person_id || '' : {};
    const user = !isEmpty(params) ? JSON.parse(params.filters).rqd_user_id || '' : {};

    const NEW_CONFIG = !isEmpty(params) ? {
      ...offset,
      ...limit,
      ...order,
      // distinct: true, // True = Avoids returns wrong count
      where: rqdId === '' ? { ...where, rqd_erased: false } : { ...where, rqd_id: rqdId, rqd_erased: false },
      include: [
        {
          model: Users, as: 'user',
          required: true, // true = (INNER JOIN || OUTER JOIN) || false = (LEFT JOIN)
          attributes: ['usr_id', 'usr_username', 'usr_phone_number', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
          include: [
            {
              model: Person,
              required: true, // true = (INNER JOIN || OUTER JOIN) || false = (LEFT JOIN)
              where: { 
                [OP.or]: [
                  { prs_name: { [OP.iLike]: `%${person}%` } },
                  { prs_lastname: { [OP.iLike]: `%${person}%` } },
                  { prs_cellphone: { [OP.iLike]: `%${person}%` } }
                ]
              },
            }
          ]
        },
        {
          model: Users, as: 'user_approve',
          attributes: ['usr_id', 'usr_username', 'usr_email'],
          required: false, // true = (INNER JOIN || OUTER JOIN) || false = (LEFT JOIN)
          where: { usr_username: { [OP.iLike]: `%${user}%` } },
        },
      ],
    } : {
      where: { ...where, rqd_erased: false, /*rqd_comment: { [OP.not]: null}*/ },
      include: [
        {
          model: Users, as: 'user',
          attributes: ['usr_id', 'usr_username', 'usr_phone_number', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
          include: [
            {
              model: Person,
            }
          ]
        },
        {
          model: Users, as: 'user_approve',
          attributes: ['usr_id', 'usr_username', 'usr_email'],
          required: false,
        },
      ],
    };

    const result = await RequestDriver.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getRequestStatus(request: Request, response: Response, usr_id: number, role_id: number) {
  try {
    const result = await RequestDriver.findOne({ where: { rqd_usr_id: request.params.id } });
    if (result.getDataValue('rqd_state') === 4) {

      const resul = await RequestDriver.findOne({
        where: { rqd_usr_id: request.params.id },
        include: [
          {
            model: Users, as: 'user',
            attributes: ['usr_id', 'usr_username', 'usr_phone_number', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
            include: [
              {
                model: Driver,
                attributes: ['dvr_id', 'dvr_usr_id', 'dvr_step_driver'],
                include: [
                  {
                    model: FeatureDriver,
                    attributes: { exclude: ['ftd_created_at', 'ftd_updated_at'] },
                    where: { ftd_state: 3 },
                    required: false,
                    // include: [
                    //   { model: FeatureType },
                    // ],
                  },
                ],
              },
            ],
          },
          {
            model: Users, as: 'user_approve',
            attributes: ['usr_id', 'usr_username', 'usr_email'],
            required: false,
          },
        ]
      });
      return response.status(200).send({status: 200, message: '¡Tu solicitud ha sido rechazada! Los motivos son:', data: resul});
    } else if (result.getDataValue('rqd_state') === 3) {
      const resul = await RequestDriver.findOne({
        where: { rqd_usr_id: request.params.id },
        include: [
          {
            model: Users, as: 'user',
            attributes: ['usr_id', 'usr_username', 'usr_phone_number', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
          },
          {
            model: Users, as: 'user_approve',
            attributes: ['usr_id', 'usr_username', 'usr_email'],
            required: false,
          },
        ]
      });
      return response.status(200).send({status: 200, message: 'Tu solicitud ya ha sido aprobada! en los próximos días, te enviaremos un mensaje por whatsapp con la fecha de lanzamiento para que descargues el APP en las tiendas', data: resul});
    } else{
      const resul = await RequestDriver.findOne({
        where: { rqd_usr_id: request.params.id },
        include: [
          {
            model: Users, as: 'user',
            attributes: ['usr_id', 'usr_username', 'usr_phone_number', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
          },
          {
            model: Users, as: 'user_approve',
            attributes: ['usr_id', 'usr_username', 'usr_email'],
            required: false,
          },
        ]
      });
      return response.status(200).send({status: 200, message: 'Tu solicitud está incompleta o pendiente', data: resul});
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await RequestDriver.findByPk(request.params.id, {
      include: [
        {
          model: Users, as: 'user',
          attributes: ['usr_id', 'usr_username', 'usr_phone_number', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
          include: [
            {
              model: Person,
              include: [
                {
                  model: Cities, as: 'cities',
                  include: [
                    {
                      model: States,
                    },
                  ]
                },
                {
                  model: DocumentType, as: 'documentType',
                  attributes: { exclude: ['dt_created_at', 'dt_updated_at'] },
                },
              ]
            },
            {
              model: Driver,
              include: [
                { model: DriverState },
                { model: DriverStatus },
                {
                  model: FeatureDriver,
                  attributes: { exclude: ['ftd_created_at', 'ftd_updated_at'] },
                  include: [
                    { model: FeatureType },
                  ],
                },
                {
                  model: FeatureVehicle,
                  attributes: { exclude: ['fv_created_at', 'fv_updated_at'] },
                  include: [
                    { model: CarModels },
                    { 
                      model: Patterns,
                      include: [
                        { model: Marks },
                      ],
                    },
                    { model: Colors },
                  ],
                },
              ],
            },
          ],
        },
        {
          model: Users, as: 'user_approve',
          attributes: { exclude: ['usr_created_at', 'usr_updated_at'] },
          required: false,
        },
      ],
    });

    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response, usr_id: number, role_id: number) {
  const payload = request.body;
  try {
    const result = await RequestDriver.findByPk(request.params.id);

    if (payload.rqd_state === 3) {
      // 3: APROBADO
      await result.update({ ...payload, rqd_user_approve_id: usr_id, rqd_updated_at: new Date(), rqd_editor_id: usr_id });
      // ---------------------------------------------
      const driver = await Driver.findOne({ where: { dvr_usr_id: result.getDataValue('rqd_usr_id') } });
      await driver.update({ dvr_status_id: 2 }); // 2: Habilitado
      // ---------------------------------------------
      const user = await Users.findByPk(result.getDataValue('rqd_usr_id'));
      await user.update({ usr_complete_step_driver: true });
      // ---------------------------------------------
      const [errSend, resultSend] = await to(sendSMS(`${user.getDataValue('usr_country_code')}${user.getDataValue('usr_phone_number')}`, `FLAPPI: La solicitud registrada en la plataforma ha sido aprobada.`));
      if (errSend) {
        return response.status(401).send({ status: 'error', message: errSend });
      }
      response.status(200).send({ status: 'success', message: 'OK' });

    } else {
      // 4: RECHAZADO
      const body = {
        // mtvd_ftd_id: payload.mtvp_mtv_id, // foto_selfi, foto_selfi_cedula, ....
        mtvd_mtv_id: payload.mtvd_mtv_id, // FOTO NO LEGIBLE
        mtvd_created_at: new Date(),
      }
      const [err, res] = await to(MotiveFeatureDriver.create(body));
      if (err) {
        return response.status(500).send({ message: err.message });
      }

      await result.update({
        ...payload,
        rqd_updated_at: new Date(),
        rqd_editor_id: usr_id,
        rqd_date_approve_reject: new Date()
      });
      // ---------------------------------------------
      const driver = await Driver.findOne({ where: { dvr_usr_id: result.getDataValue('rqd_usr_id') } });
      await driver.update({ dvr_status_id: 3 }); //  3: Deshabilitado
      // ---------------------------------------------
      const user = await Users.findByPk(result.getDataValue('rqd_usr_id'));
      await user.update({ usr_complete_step_driver: false });
      // ---------------------------------------------
      // ---------------------------------------------
      const [errSend, resultSend] = await to(sendSMS(`${user.getDataValue('usr_country_code')}${user.getDataValue('usr_phone_number')}`,
        `FLAPPI: La solicitud registrada en la plataforma ha sido rechazada. Por favor ingresa aqui App FLAPPI y actualiza la información.`));
      if (errSend) {
        return response.status(401).send({ status: 'error', message: errSend });
      }
      response.status(200).send({ status: 'success', message: 'OK' });
    }

  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await RequestDriver.findByPk(request.params.id);
    // const data = await result.destroy();
    const data = await result.update({ rqd_erased: true });
    response.status(200).send({ message: 'success' });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function fileList(request: Request, response: Response, usr_id: number, role_id: number) {
  try {
    const order = orderModel('ftd_id', 'ASC');
    const req = await RequestDriver.findByPk(request.params.id);
    const driver = await Driver.findOne({ where: { dvr_usr_id: req.getDataValue('rqd_usr_id') } });
    const feature = await FeatureDriver.findAndCountAll({
      ...order,
      where: { ftd_dvr_id: driver.getDataValue('dvr_id') }
    });
    response.status(200).send(feature);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function fileState(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  try {
    const result = await FeatureDriver.findByPk(request.params.id);
    const body = {
      ...payload,
      ftd_editor_id: usr_id,
      ftd_updated_at: new Date(),
    };
    await result.update(body);
    response.status(200).send({ message: 'success' });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}