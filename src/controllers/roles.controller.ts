/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Roles from '../models/dgRoles';

/************ Functions ************/
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  try {

    const params: any = request.query;
    const limit = { limit: Number(params.rows) || 20 };
    const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
    const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
    // ---------------------------------------------------------------------------------
    // delete foreign key
    const exclude_keys = [
      { key_id: 'rle_id' }
    ];
    const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
    // ---------------------------------------------------------------------------------
    const rleId = !isEmpty(params) ? JSON.parse(params.filters).rle_id || '' : {};

    const NEW_CONFIG = !isEmpty(params) ? {
      ...offset,
      ...limit,
      ...order,
      where: rleId === '' ? where : { ...where, rle_id: rleId },
      attributes: { exclude: ['rle_created_at', 'rle_updated_at'] },
    } : {
      where,
      attributes: { exclude: ['rle_created_at', 'rle_updated_at'] },
    };

    const result = await Roles.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await Roles.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
/**
 * Creates a new role.
 *
 * @param request - The HTTP request object.
 * @param response - The HTTP response object.
 * @returns A Promise that resolves to the created role.
 */
export async function create(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Roles.create({...payload, rle_created_at: new Date(), rle_creator_id: 1});
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Roles.findByPk(request.params.id);
    const data = await result.update({...payload, rle_updated_at: new Date()});
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await Roles.findByPk(request.params.id);
    const data = await result.destroy();
    response.status(200).send({ message: 'success' });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getEmployee(request: Request, response: Response) {
  try {
    const result = await Roles.findAll({
      where: { rle_id: { [OP.notIn]: [1, 2] }, rle_state: 1 },// 1: superadmin y 2: person (conductor/pasajero)
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}