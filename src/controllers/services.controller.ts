/************ import libraries ************/
import { isEmpty } from 'lodash';
import to from 'await-to-js';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import Users from '../models/dgUsers';
import Passenger from '../models/passenger';
import Driver from '../models/driver';
import Services from '../models/services';
import ServicesState from '../models/servicesState';
import PaymentMethods from '../models/paymentMethods';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to create services
 * @param request
 * @param response
 */
export async function getPassengerById(request: Request, response: Response, usr_id: number) {
  try {
    const data = await Users.findByPk(request.params.id);
    const dataPassemger = await Passenger.findOne({ where: { psg_usr_id: data.getDataValue('usr_id') } });

    const order = orderModel('srv_updated_at', 'DESC');
    const NEW_CONFIG = {
      where: { srv_passenger_id: dataPassemger.getDataValue('psg_id'), srv_state_id: { [OP.or]: [5, 6, 7, 8] }, srv_erased: false },
      attributes: { exclude: ['srv_erased'] },
      ...order,
      include: [
        {
          model: Passenger, as: 'passenger',
          attributes: ['psg_id', 'psg_discount', 'psg_trips', 'psg_usr_id', 'psg_status_id'],
          include: [
            {
              model: Users,
              attributes: ['usr_id', 'usr_mode', 'usr_username', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],

            }
          ]
        },
        {
          model: ServicesState,
        },
        {
          model: PaymentMethods,
        }
      ],
    };
    const result = await Services.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create services
 * @param request
 * @param response
 */
export async function getDriverById(request: Request, response: Response, usr_id: number) {
  try {
    const order = orderModel('srv_updated_at', 'DESC');
    const data = await Users.findByPk(request.params.id);
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
    const NEW_CONFIG = {
      where: { srv_driver_id: dataDriver.getDataValue('dvr_id'), srv_state_id: { [OP.or]: [5, 6, 7, 8] }, srv_erased: false },
      attributes: { exclude: ['srv_erased'] },
      ...order,
      include: [
        {
          model: Driver, as: 'driver',
          attributes: ['dvr_id', 'dvr_discount', 'dvr_trips', 'dvr_usr_id', 'dvr_state_id', 'dvr_status_id'],
          include: [
            {
              model: Users,
              attributes: ['usr_id', 'usr_mode', 'usr_username', 'usr_avatar', 'usr_country_code', 'usr_phone_number'],
            }
          ]
        },
        {
          model: ServicesState,
        },
        {
          model: PaymentMethods,
        }
      ],
    };
    const result = await Services.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}