/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import States from '../models/states';

/************ Functions ************/
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;


/**
 * @author Randall Medina
 * @description Function class to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  try {

    const params: any = request.query;
    const limit = { limit: Number(params.rows) || 20 };
    const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
    const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
    // ---------------------------------------------------------------------------------
    // delete foreign key
    const exclude_keys = [
      { key_id: 'dpr_id' }
    ];
    const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
    // ---------------------------------------------------------------------------------
    const dprId = !isEmpty(params) ? JSON.parse(params.filters).dpr_id || '' : {};

    const NEW_CONFIG = !isEmpty(params) ? {
      ...offset,
      ...limit,
      ...order,
      where: dprId === '' ? where : { ...where, dpr_id: dprId },
    } : {
      where
    };
    const result = await States.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function class to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await States.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function class to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await States.create(payload);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function class to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await States.findByPk(request.params.id);
    const data = await result.update(payload);
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function class to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await States.findByPk(request.params.id);
    if (result) {
      const data = await result.destroy();
      response.status(200).send({ message: 'success' });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
