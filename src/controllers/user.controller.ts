/************ import libraries ************/
import to from 'await-to-js';
import { isEmpty, isNil } from 'lodash';
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import * as Crypto from 'crypto';
import Sequelize = require('sequelize');
import * as AWS from 'aws-sdk';
import path = require('path');
import fs = require('fs');

/** import models */
import Users from '../models/dgUsers';
import Person from '../models/person';
import userRoles from '../models/userRoles';

/************ Functions ************/
import sequelizeQuery from '../db-config';
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';
import { getHash, compareHash } from '../functions/Hash';
import { createFileS3, deleteFileS3, getFileS3 } from '../functions/uploadFile';
/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {
  try {
    const params: any = request.query;
    const limit = { limit: Number(params.rows) || 20 };
    const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
    const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
    // ---------------------------------------------------------------------------------
    // delete foreign key
    const exclude_keys = [
      { key_id: 'usr_id' }
    ];
    const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
    // ---------------------------------------------------------------------------------
    const usrId = !isEmpty(params) ? JSON.parse(params.filters).usr_id || '' : {};

    const NEW_CONFIG = !isEmpty(params) ? {
      ...offset,
      ...limit,
      ...order,
      where: usrId === '' ? { ...where, usr_erased: false } : { ...where, usr_id: usrId, usr_erased: false },
      attributes: ['usr_id', 'usr_username', 'usr_registration_ip', 'usr_origin', 'usr_erased', 'usr_state_login'],
      include: [
        {
          model: userRoles, as: 'userRoles',
        },
      ],
    } : {
      where: { ...where, usr_erased: false },
      attributes: ['usr_id', 'usr_username', 'usr_registration_ip', 'usr_origin', 'usr_erased', 'usr_state_login'],
    };

    const result = await Users.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await Users.findByPk(request.params.id, {
      include: [
        {
          model: Person, as: 'person',
        },
      ],
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response) {
  const payload = request.body;
  const body = { ...payload.person };
  delete payload.person;
  try {
    const verify = await Users.findAll({ where: { usr_username: payload.usr_username } });
    if (verify[0]) {
      response.status(500).send({ status: 'error', message: 'usuario ya existe' });
    } else {
      const password = await getHash(payload.usr_password);
      const result = await Users.create({ ...payload, usr_password: password });
      const bodyPerson = { ...body, prs_usr_id: result.getDataValue('usr_id') };
      const [err, res] = await to(Person.create(bodyPerson));
      if (err) {
        const delU = await result.destroy();
        return response.status(500).send({ status: 'error', message: err.message });
      } 
      response.status(200).send({ ...result['dataValues'], ...res['dataValues'] });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await Users.findByPk(request.params.id, { attributes: { exclude: ['usr_password'] } });
    if (payload.usr_password) {

      const password = await getHash(payload.usr_password);
      const data = await result.update({ ...payload, usr_password: password });
      response.status(200).send(data);

    } else {

      const data = await result.update(payload);
      response.status(200).send(data);
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await Users.findByPk(request.params.id);
    const data = await result.destroy();
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function updatePhoto(request: Request, response: Response) {
  const payload = request.body;
  const file = request.file;
  try {
    if (!file) {
      return response.status(400).send({ status: 'error', message: 'No se ha seleccionado ningún archivo.' });
    }
    // -------------------------------------------------
    // Consultar usuario
    const data = await Users.findByPk(request.body.usr_id);
    if (!data) {
      return response.status(500).send({ status: 'error', message: '¡El usuario no existe!' });
    }
    // -------------------------------------------------
    const url = `conductores/${data.getDataValue('usr_id')}`
    // -------------------------------------------------
    // Verificar si la foto esta en s3 
    if (data.getDataValue('usr_avatar')) {
      const fotoExiste = await getFileS3(url, data.getDataValue('usr_avatar'));
      // console.log('fotoExiste', fotoExiste)
      if (fotoExiste) {
        // Borrar la foto vieja del S3
        const fotoOld = data.getDataValue('usr_avatar');
        await deleteFileS3(url, fotoOld);
      }
    }
    // -------------------------------------------------
    const fotoCargada = await createFileS3(file, url, payload.usr_avatar);
    if (!fotoCargada) {
      return response.status(500).send({ status: 'error', message: '¡Error al subir la foto a S3!' });
    }
    // -------------------------------------------------
    const resp = await data.update({ usr_avatar: payload.usr_avatar });
    delete resp['dataValues']['usr_password'];
    response.status(200).send(resp);
    // -------------------------------------------------
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function uploadFile(request: Request, response: Response, usr_id: number) {
  try {

    const file = request.file;
    const payload = request.body;
    console.log('file ==========', file)
    if (!file) {
      return response.status(400).send({ status: 'error', message: 'No se ha seleccionado ningún archivo.' });
    }
    // --------------------------------------------
    // --------------------------------------------
    // const tempFilePath = path.join(__dirname, '../../uploads', `tempfile_${Date.now()}.jpg`);
    // // Asegúrate de que la carpeta temporal exista
    // const tempDir = path.dirname(tempFilePath);
    // if (!fs.existsSync(tempDir)) {
    //   fs.mkdirSync(tempDir, { recursive: true });
    // }

    // // // Lee el contenido del archivo original y escribe en el archivo temporal
    // fs.copyFileSync(file.path, tempFilePath);

    // // // Lee el contenido del archivo temporal
    // const dataPath = fs.readFileSync(tempFilePath, 'utf-8');

    // --------------------------------------------
    // --------------------------------------------
    const data = await Users.findByPk(request.body.usr_id);
    if (!data) {
      return response.status(500).send({ status: 'error', message: '¡El usuario no existe!' });
    }
    // --------------------------------------------
    // --------------------------------------------
    // const buf = new Buffer(params.user.image.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    // const buf: any = await readableToBuffer(file);
    const randomFile = await Crypto.randomBytes(30).toString('hex');
    const nameFile = `${randomFile}.jpeg`;
    const url = `conductores/${payload.usr_id}`
    await createFileS3(file, url, nameFile);

    response.status(200).send({ status: 'success', message: '¡Archivo creado!', data: { file_hash: nameFile, usr_id: data.getDataValue('usr_id') } });

  } catch (error) {
    console.log('error uploadFile ===', error)
    response.status(500).send({ message: error.message });
  }
}

async function readableToBuffer(readable) {
  return new Promise((resolve) => {
    let chunks = [];

    readable.on('data', (chunk) => {
      chunks.push(chunk);
    });

    readable.on('end', () => {
      resolve(Buffer.concat(chunks));
    });
  });
}