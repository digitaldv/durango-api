/************ import libraries ************/
import { isEmpty } from 'lodash';
import Sequelize = require('sequelize');
import { Request, Response } from 'express';

/************ import models ************/
import CarModels from '../models/carModels';
/************ Functions ************/
import { orderModel } from '../functions/orderModel';
import { whereModel } from '../functions/whereModel';

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function to get all
 * @param request
 * @param response
 */
export async function getAll(request: Request, response: Response) {

  const params: any = request.query;
  const limit = { limit: Number(params.rows) || 20 };
  const offset = { offset: ((Number(params.page) || 0) * (Number(params.rows) || 20)) };
  const order = !isEmpty(params.filters) ? orderModel(params.order_field, params.order_type) : {};
  // ---------------------------------------------------------------------------------
  // delete foreign key
  const exclude_keys = [
    { key_id: 'crm_id' }
  ];
  const where = await whereModel(!isEmpty(params.filters) ? JSON.parse(params.filters) : {}, exclude_keys);
  // ---------------------------------------------------------------------------------
  const crmId = !isEmpty(params) ? JSON.parse(params.filters).crm_id || '' : {};

  const NEW_CONFIG = !isEmpty(params) ? {
    ... offset,
    ... limit,
    ... order,
    where: crmId === '' ? {...where, crm_erased: false} : { ...where, crm_id: crmId, crm_erased: false },
    // attributes: { exclude: ['crm_erased'] },
  } : {
    where : { ...where, crm_erased: false, crm_state: 1 },
  };

  try {
    const result = await CarModels.findAndCountAll(NEW_CONFIG);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getById(request: Request, response: Response) {
  try {
    const result = await CarModels.findByPk(request.params.id);
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to create
 * @param request
 * @param response
 */
export async function create(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await CarModels.create({...payload, crm_created_at: new Date(), crm_erased: false});
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to update
 * @param request
 * @param response
 */
export async function update(request: Request, response: Response) {
  const payload = request.body;
  try {
    const result = await CarModels.findByPk(request.params.id);
    const data = await result.update({...payload, crm_updated_at: new Date()});
    response.status(200).send(data);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}
/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function delet(request: Request, response: Response) {
  try {
    const result = await CarModels.findByPk(request.params.id);
    if (result) {
      const data = await result.update({ crm_erased: true });
      // const data = await result.destroy();
      response.status(200).send({ message: 'success' });
    }
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}