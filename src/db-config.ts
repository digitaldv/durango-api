import { Sequelize } from 'sequelize-typescript';

export default new Sequelize({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT as any || 5432,
  database: process.env.DB_NAME,
  dialect: 'postgres',
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  modelPaths: [`${__dirname}/models`],
  logging:false,
  define: {
    underscored: true,
    charset: 'utf8',
    timestamps: true,
    schema: "dba_new_db",
    // schema: "public",
  },
  // timezone: 'America/Bogota'
  timezone: "-05:00"
});
