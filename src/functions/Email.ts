class Email {
  private nodemailer = require('nodemailer');
  private fs = require('fs');
  private mustache = require('mustache');
  private async getUser(test= false) {
    if (test) {
      const testAccount = await this.nodemailer.createTestAccount();
      return {
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false,
        auth:{
          user: testAccount.user,
          pass: testAccount.pass,
        },
      };
    }
    return {
      host: 'smtp.sendgrid.net',
      port: 587,
      secure: false,
      auth:{
        user: 'apikey', // APIKeyID : E_ecdytxTp60m2Oemil8wA
        pass: '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',
      },
    };
  }

 // tslint:disable-next-line:member-ordering
  public async sendEmail(email, pass, role, phone, test= false) {
    try {
      let subject;
      let page;
      if (role === 2){
        // pasajero = 2
        subject = '¡Bienvenido a FLAPPI! Te haz registrado como pasajero.';
        page = 'resources/pasajero.html';
      } else {
        // conductor = 3
        subject = '¡Bienvenido a FLAPPI! Te haz registrado como conductor.';
        page = 'resources/conductor.html';
      }
      const colTime =  new Date().toLocaleString('es-CO', { timeZone: 'America/Bogota' });
      // const hoy = `${fecha.getFullYear()}-${fecha.getMonth() + 1}-${fecha.getDate()}`
      const connectionConfig = await this.getUser(test);
      const transporter = this.nodemailer.createTransport(connectionConfig);
      // const template = this.fs.readFileSync(`${__dirname}/resources/register.html`, 'utf8');
      const template = this.fs.readFileSync(`${__dirname}/${page}`, 'utf8');
      const user_info = { password: pass, user: phone};
      const html = this.mustache.render(template, user_info);
      const emailData = {
        html, // html body
        from: 'Info FLAPPI <no-responder@durangoapp.com>', // sender address
        to: `${email}`, // list of receivers
        subject: `${subject} - ${colTime}`, // Subject line
        text: role !== 2 ? `Usuario ${email} / contrasena : ${pass}` : `Usuario ${email}` , // plain text body
      };
      const info = await transporter.sendMail(emailData);
      console.log('Message sent: %s', info.messageId);
      console.log('Preview URL: %s', this.nodemailer.getTestMessageUrl(info));
    }catch (e) {
      console.log('Error in email %s', e);
    }
  }

  // tslint:disable-next-line:member-ordering
  public async sendEmailRequest(email, data, type, test= false) {
    try {
      let subject;
      let page;
      if(type === 'nueva'){
        subject = '¡Se ha creado una solicitud!.';
        page = 'resources/solicitud.html';
      }else{
        subject = '¡Se ha editado una solicitud!.';
        page = 'resources/solicitud.html'

      }
      const colTime =  new Date().toLocaleString('es-CO', { timeZone: 'America/Bogota' });
      // const hoy = `${fecha.getFullYear()}-${fecha.getMonth() + 1}-${fecha.getDate()}`
      const connectionConfig = await this.getUser(test);
      const transporter = this.nodemailer.createTransport(connectionConfig);
      // const template = this.fs.readFileSync(`${__dirname}/resources/register.html`, 'utf8');
      const template = this.fs.readFileSync(`${__dirname}/${page}`, 'utf8');
      // const user_info = { user: data};
      const html = this.mustache.render(template, data);
      const emailData = {
        html, // html body
        from: 'Info FLAPPI <no-responder@durangoapp.com>', // sender address
        to: `${email}`, // list of receivers
        // cc: ['info@reunidos.app', 'juandavid@reunidos.app', 'jorge.osborne@reunidos.app'],
        subject: `${subject} - ${colTime}`, // Subject line
        text: `Conducor ${data.name} ${data.lastname} - Celular: ${data.phone}` , // plain text body
      };
      const info = await transporter.sendMail(emailData);
      console.log('Message sent: %s', info.messageId);
      console.log('Preview URL: %s', this.nodemailer.getTestMessageUrl(info));
    }catch (e) {
      console.log('Error in email %s', e);
    }
  }

}
export default new Email();
