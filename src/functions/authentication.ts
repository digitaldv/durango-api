/************ import libraries ************/
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import to from 'await-to-js';
import { isEmpty, isNil } from 'lodash';
import Sequelize = require('sequelize');

/************ Import models ************/
import Users from '../models/dgUsers';
import Roles from '../models/dgRoles';
import UserRoles from '../models/userRoles';
import SmsHistory from '../models/smsHistory';
import Driver from '../models/driver';
import Passenger from '../models/passenger';
import Params from '../models/params';

/************ import function to compare hash and tokenHeader ************/
import { compareHash, getTokenFromHeader, getHash } from './Hash';

/************ Import .env ************/
const { SECRET } = process.env;

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

import { sendSMS } from './sendMessage';

/**
 * @author Randall Medina
 * @description The middleware takes any controller in the API and validates
 * the Fernet token (if it's valid) and returns the response for the desired
 * action in the controller, if it's not valid, it returns and error object in the response.
 * @param {any} controller
 * @param  {Response} response
 * @param  {Request} request
 * @returns {Promise}
 */
export const authMiddleware = (controller: any) => async (request: Request, response: Response) => {
  const token = getTokenFromHeader(request.headers.authorization || '');
  await jwt.verify(token, SECRET, async (error: Error, decoded) => {
    if (error) {
      response.status(401).send({ status: 'error', message: error.message });
    } else {
      const { usr_id, usr_rol_id } = decoded;
      await controller(request, response, usr_id, usr_rol_id);
    }
  });
};

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function sendCodeAuth(request: Request, response: Response) {
  try {
    const { phone, country_code } = request.body;
    const code: any = Math.floor(Math.random() * 1000 + 1000);
    // const codeHash = await getHash(code.toString());
    const NEW_CONFIG = {
      where: { usr_phone_number: phone, usr_country_code: country_code },
    };
    const data = await Users.findOne(NEW_CONFIG);
    if (data) {
      if (!data.getDataValue('usr_erased')) {
        const result = await Users.findByPk(data.getDataValue('usr_id'));
        const user = await result.update({ usr_sms_code: code, usr_updated_at: new Date() });

        const [errSend, resultSend] = await to(sendSMS(`${country_code}${phone}`, `*CÓDIGO FLAPPI:* ${code}`));
        if (errSend) {
          return response.status(401).send({ status: 'error', message: errSend });
        }
        const [errSms, resultSms] = await to(SmsHistory.create({ smsh_usr_id: user.usr_id, smsh_code: code, smsh_created_at: new Date() }));
        if (errSms) {
          return response.status(401).send({ status: 'error', message: errSms });
        }
        response.status(200).send({ phone: `${country_code}${phone}`, message: 'success' });

      } else {
        response.status(500).send({ status: 'error', message: '¡El usuario está bloqueado!' })
      }
    } else {
      response.status(500).send({ status: 'error', message: '¡El usuario no existe!' });
    }
  } catch (err) {
    response.status(401).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function authenticateCode(request: Request, response: Response) {
  try {
    const { phone, origin, country_code } = request.body;
    // --------------------------------------------------------
    const code: any = Math.floor(Math.random() * 1000 + 1000);
    // const codeHash = await getHash(code.toString());
    const NEW_CONFIG = {
      where: { usr_phone_number: phone },
    };
    const data = await Users.findOne(NEW_CONFIG);
    // --------------------------------------------------------
    if (data) {
      // Existe el usuario
      if (data.getDataValue('usr_erased')) {
        response.status(401).send({ status: 'error', action: 'erased', message: '¡Usuario bloqueado!' });
      } else {
        const user = await data.update({ usr_sms_code: code });
        const [errSend, resultSend] = await to(sendSMS(`${country_code}${phone}`, `*CÓDIGO FLAPPI:* ${code}`));
        if (errSend) {
          return response.status(401).send({ status: 'error', message: errSend });
        }

        const [errSms, resultSms] = await to(SmsHistory.create({ smsh_usr_id: user.usr_id, smsh_code: code, smsh_created_at: new Date() }));
        if (errSms) {
          return response.status(401).send({ status: 'error', message: errSms });
        }
        response.status(200).send({ phone: `${country_code}${phone}`, message: 'success' });
      }

    } else {

      const bodyUser: any = {
        usr_origin: origin,
        usr_sms_code: code,
        usr_phone_number: phone,
        usr_country_code: country_code,
        usr_creator_id: 1,
        usr_created_at: new Date(),
        usr_verified: 0,
        usr_erased: false
      };
      const [err, result] = await to(Users.create(bodyUser));
      if (err) {
        return response.status(500).send({ status: err.message, message: err });
      }
      const [errSend, resultSend] = await to(sendSMS(`${country_code}${phone}`, `*CÓDIGO FLAPPI:* ${code}`));
      if (errSend) {
        return response.status(401).send({ status: 'error', message: errSend });
      }
      const [errSms, resultSms] = await to(SmsHistory.create({ smsh_usr_id: result.getDataValue('usr_id'), smsh_code: code, smsh_created_at: new Date() }));
      if (errSms) {
        return response.status(401).send({ status: 'error', message: errSms });
      }
      response.status(200).send({ phone: `${country_code}${phone}`, message: 'success' });

    }
  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description it receives the authentication info and generate a JWT token
 * it return the JWT token in the response object.
 * if auth info is incorrect, it sends a 401 status error in response
 * @param {body} {body} object with user information
 * @param  {Response} response
 * @param  {Request} request
 * @returns {Promise}
 */
export async function verifyAuthenticate(request: Request, response: Response) {
  try {
    const { phone, code, country_code } = request.body;
    // --------------------------------------------------------
    const NEW_CONFIG = {
      where: { usr_phone_number: phone, usr_country_code: country_code },
      attributes: {
        exclude: [
          'usr_registration_ip',
          'usr_origin',
          'usr_created_at',
          'usr_updated_at',
          'usr_creator_id',
          'usr_editor_id',
        ],
      },
      include: [
        {
          model: UserRoles, as: 'userRoles',
          required: false, // true = (INNER JOIN || OUTER JOIN) || false = (LEFT JOIN)
          include: [
            {
              model: Roles, as: 'role',
              attributes: {
                exclude: ['rle_created_at', 'rle_updated_at'],
              },
            },
          ],
        },
      ],
    };
    const data = await Users.findOne(NEW_CONFIG);
    // --------------------------------------------------------
    if (data) {
      if (!data.getDataValue('usr_erased')) {
        if (code === data.getDataValue('usr_sms_code')) {
          //--------------------------------------------
          delete data['dataValues']['usr_password'];
          const access_token = jwt.sign({
            usr_id: data.getDataValue('usr_id'), usr_rol_id: data.getDataValue('userRoles') && data.getDataValue('userRoles')['usrl_rle_id']
          }, SECRET, {
            expiresIn: '7d'
          });
          //--------------------------------------------
          if (Number(data.getDataValue('usr_verified')) === 0) {
            await data.update({ usr_verified: 1, usr_sms_code: null, usr_updated_at: new Date(), usr_state_login: 1 });
          } else {
            await data.update({ usr_sms_code: null, usr_state_login: 1 });
          }
          //--------------------------------------------
          // Verificar si existe un registro Driver/Passenger pendiente por llenar la informacion
          const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
          const dataPassenger = await Passenger.findOne({ where: { psg_usr_id: data.getDataValue('usr_id') } });
          //--------------------------------------------
          // Obtener el porcentaje de descuento a apllicar Driver/Passenge (id: 20 - porcentaje_descuento_pasajero)
          const dataParams = await Params.findByPk(20);
          //--------------------------------------------
          const user = {
            ...data['dataValues'],
            role: data.getDataValue('userRoles') && data.getDataValue('userRoles')['role'],
            step_driver: dataDriver ? dataDriver.getDataValue('dvr_step_driver') : null,

            driver_id: dataDriver ? dataDriver.getDataValue('dvr_id') : null,
            passenger_id: dataPassenger ? dataPassenger.getDataValue('psg_id') : null,
            
            driver_discount: dataDriver ? dataDriver.getDataValue('dvr_discount') : null,
            passenger_discount: dataPassenger ? dataPassenger.getDataValue('psg_discount') : null,

            percentage_discount: dataParams ? dataParams.getDataValue('prm_value') : null,

            data: data.getDataValue('usr_origin') === 'WEB' ? {
              displayName: data.getDataValue('usr_username'),
              // photoURL: 'assets/images/avatars/Shauna.jpg',
              // photoURL: result['dataValues'].usr_avatar,
              photoURL: null,
              email: data.getDataValue('usr_email'),
              settings: {
                layout: {
                  style: 'layout1',
                  config: {
                    //   scroll: 'content',
                    navbar: {
                      display: true,
                      folded: false,
                      //     position: 'left',
                    },
                    toolbar: {
                      display: true,
                      // style: 'fixed',
                      // position: 'below',
                    },
                    //   footer: {
                    //     display: false,
                    //     style: 'fixed',
                    //     position: 'below',
                    //   },
                    //   mode: 'fullwidth',
                  },
                },
                customScrollbars: true,
                theme: {
                  main: 'default',
                  navbar: 'mainThemeDark',
                  toolbar: 'mainThemeLight',
                  footer: 'mainThemeDark',
                },
              },
            } : {},
          };
          //--------------------------------------------
          response.status(200).send({
            status: 'success', message: '¡Usuario logueado!',
            auth_info: {
              user,
              access_token,
            },
          });
        } else {
          response.status(500).send({ status: 'error', message: '¡Código invalido!' });
        }
      } else {
        response.status(500).send({ status: 'error', message: '¡El usuario está bloqueado!' });
      }
    } else {
      response.status(500).send({ status: 'error', message: '¡El usuario no existe!' });
    }
  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @param  {{body:IUser}} {body}
 * @param  {Response} response
 * @description it receives the user authentication info and generates a JWT token
 * it return the JWT token in the response object. If auth info is incorrent, it
 * sends a 401 status error in response
 *
 */
export async function authenticateToken(request: Request, response: Response) {
  try {
    const token = getTokenFromHeader(request.headers.authorization || '');
    const { usr_id, usr_rol_id } = jwt.verify(token, SECRET);
    const NEW_CONFIG = {
      where: { usr_id, usr_erased: false, usr_state_login: 1 },
      attributes: {
        exclude: [
          'usr_registration_ip',
          'usr_origin',
          'usr_created_at',
          'usr_updated_at',
          'usr_creator_id',
          'usr_editor_id',
        ],
      },
      include: [
        {
          model: UserRoles, as: 'userRoles',
          required: false, // false: to force a LEFT JOIN, true: forces an INNER JOIN
          include: [
            {
              model: Roles, as: 'role',
              attributes: {
                exclude: ['rle_created_at', 'rle_updated_at'],
              },
            },
          ],
        },
      ],
    };
    const result = await Users.findOne(NEW_CONFIG);
    delete result['dataValues']['usr_password'];
    const updatedAccessToken = jwt.sign({
      usr_id: result.getDataValue('usr_id'),
      usr_rol_id: result.getDataValue('userRoles') && result.getDataValue('userRoles')['usrl_rle_id']
    }, SECRET, { expiresIn: '7d' });
    //--------------------------------------------
     // Verificar si existe un registro Driver/Passenger pendiente por llenar la informacion
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: result.getDataValue('usr_id') } });
    const dataPassenger = await Passenger.findOne({ where: { psg_usr_id: result.getDataValue('usr_id') } });
    //--------------------------------------------
    // Obtener el porcentaje de descuento a apllicar Driver/Passenge (id: 20 - porcentaje_descuento_pasajero)
    const dataParams = await Params.findByPk(20);
    //--------------------------------------------
    const user = {
      ...result['dataValues'],
      role: result.getDataValue('userRoles') && result.getDataValue('userRoles')['role'],
      step_driver: dataDriver ? dataDriver.getDataValue('dvr_step_driver') : null,

      driver_id: dataDriver ? dataDriver.getDataValue('dvr_id') : null,
      passenger_id: dataPassenger ? dataPassenger.getDataValue('psg_id') : null,

      driver_discount: dataDriver ? dataDriver.getDataValue('dvr_discount') : null,
      passenger_discount: dataPassenger ? dataPassenger.getDataValue('psg_discount') : null,

      percentage_discount: dataParams ? dataParams.getDataValue('prm_value') : null,
    };

    const data = {
      // ... result['dataValues'],
      data: result.getDataValue('usr_origin') === 'WEB' ? {
        displayName: result.getDataValue('usr_username'),
        // photoURL: 'assets/images/avatars/Shauna.jpg',
        // photoURL: result['dataValues'].usr_avatar,
        photoURL: null,
        email: result.getDataValue('usr_email'),
        settings: {
          layout: {
            style: 'layout1',
            config: {
              //   scroll: 'content',
              navbar: {
                display: true,
                folded: false,
                //     position: 'left',
              },
              toolbar: {
                display: true,
                // style: 'fixed',
                // position: 'below',
              },
              //   footer: {
              //     display: false,
              //     style: 'fixed',
              //     position: 'below',
              //   },
              //   mode: 'fullwidth',
            },
          },
          customScrollbars: true,
          theme: {
            main: 'default',
            navbar: 'mainThemeDark',
            toolbar: 'mainThemeLight',
            footer: 'mainThemeDark',
          },
        },
      } : {},
    };
    response.status(200).send({
      auth_info: {
        user: { ...user, ...data },
        // user :data,
        access_token: updatedAccessToken,
      },
    });

  } catch (error) {
    response.status(401).send({ status: 'error', message: error });
  }
}

export async function verifyTokenAuth(request: Request, response: Response) {

  const token = getTokenFromHeader(`Bearer ${request.body.token}`);
  await jwt.verify(token, SECRET, async (error: Error, decoded) => {
    if (error) {
      response.status(401).send({ status: 401, code: 'error', message: error.message });
    } else {
      const { usr_id, usr_rol_id } = decoded;
      const result = await Users.findOne({
        where: { usr_id },
        include: [
          {
            model: UserRoles, as: 'userRoles',
            required: false, // true = (INNER JOIN || OUTER JOIN) || false = (LEFT JOIN)
            where: { usrl_rle_id: usr_rol_id },
            include: [
              {
                model: Roles, as: 'role',
              },
            ],
          }
        ]
      });
      if (result) {
        delete result['dataValues']['usr_password'];
        return response.status(200).send({ status: 200, code: 'success', message: '¡Token valido!', data: result });
      } else {
        response.status(500).send({ status: 500, code: 'error', message: '¡Usuario no existe!' });
      }
    }
  });
};

/**
 * @author Randall Medina
 * @description it receives the authentication info and generate a Fernet token
 * it return the Fernet token in the response object.
 * if auth info is incorrect, it sends a 401 status error in response
 * @param {body} {body} object with user information
 * @param  {Response} response
 * @param  {Request} request
 * @returns {Promise}
 */
export async function authenticateWeb(request: Request, response: Response) {
  try {
    const { username, password } = request.body;

    const NEW_CONFIG = {
      where: { usr_email: username, usr_erased: false },
      attributes: {
        exclude: [
          'usr_registration_ip',
          'usr_origin',
          'usr_created_at',
          'usr_updated_at',
          'usr_creator_id',
          'usr_editor_id',
        ],
      },
      include: [
        {
          model: UserRoles, as: 'userRoles',
          required: false, // true = (INNER JOIN || OUTER JOIN) || false = (LEFT JOIN)
          include: [
            {
              model: Roles, as: 'role',
              attributes: {
                exclude: ['rle_created_at', 'rle_updated_at'],
              },
            },
          ],
        },
      ],
    };
    const result = await Users.findOne(NEW_CONFIG);
    const passwordMatch = await compareHash(
      password,
      result ? result.getDataValue('usr_password') : '',
    );
    if (result && passwordMatch) {
      delete result['dataValues']['usr_password'];
      const access_token = jwt.sign({
        usr_id: result.getDataValue('usr_id'), usr_rol_id: result.getDataValue('userRoles') && result.getDataValue('userRoles')['usrl_rle_id']
      }, SECRET, {
        expiresIn: '7d'
      });

      await result.update({ usr_state_login: 1 })

      const user = {
        ...result['dataValues'],
        role: result.getDataValue('userRoles') && result.getDataValue('userRoles')['role'],
      };
      const data = {
        // ... result['dataValues'],
        data: {
          displayName: result.getDataValue('usr_username'),
          // photoURL: 'assets/images/avatars/Shauna.jpg',
          // photoURL: result['dataValues'].usr_avatar,
          photoURL: null,
          email: result.getDataValue('usr_email'),
          settings: {
            layout: {
              style: 'layout1',
              config: {
                //   scroll: 'content',
                navbar: {
                  display: true,
                  folded: false,
                  //     position: 'left',
                },
                toolbar: {
                  display: true,
                  // style: 'fixed',
                  // position: 'below',
                },
                //   footer: {
                //     display: false,
                //     style: 'fixed',
                //     position: 'below',
                //   },
                //   mode: 'fullwidth',
              },
            },
            customScrollbars: true,
            theme: {
              main: 'default',
              navbar: 'mainThemeDark',
              toolbar: 'mainThemeLight',
              footer: 'mainThemeDark',
            },
          },
        },
      };

      response.status(200).send({
        auth_info: {
          access_token,
          user: { ...user, ...data },
          // user :data,
        },
      });
    } else {
      response.status(401).send({ status: 'error', message: 'Error de datos' });
    }
  } catch (error) {
    response.status(401).send({ status: 'error', message: error });
  }
}