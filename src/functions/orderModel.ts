/************ import models ************/
import States from '../models/states';
import Marks from '../models/marks';

/**
 * @author Randall Medina
 * @description Function to order model
 * if receive an object to organize the variables and returns an object to order the condition
 * @param {string} order_field
 * @param {string} order_type
 */
// export const orderModel = (order_field: any, order_type: any) => {
//   let order = [];
//   switch (order_field) {
//     // // ================================= order for Orders
//     // case 'ord_cli_phone':
//     //   // 2 niveles
//     //   order = [[ { model: Users, as : 'creator' }, { model: Customers, as: 'customers' }, 'cli_phone', order_type]]
//     //   break;

//     // ================================= order for city
//     case 'dpr_name':
//       order = [[{ model: States, as: 'states' }, 'dpr_name', order_type]];
//       break;
//     // ================================= order for patterns
//     case 'pat_mk_id':
//       order = [[{ model: Marks, as: 'mark' }, 'mk_name', order_type]];
//       break;

//     // ================================= default table
//     default:
//       order = [[order_field, order_type]];
//       break;
//   }
//   return { order };
// };

export const orderModel = (order_field, order_type) => {
  let order = [];
  switch (order_field) {
    // ================================= default table
    default:
      order = [[order_field, order_type]];
      break;
  }
  return { order };
};

/**
 * @author Styk Medina
 * @param order_field define el parametro con el que voy a ordernar o comparar para ordenar por sub niveles de modelos
 * @param order_type define el order sea ASC o DESC
 * @param sort_keys array de modelos con alias para odernar por sub niveles de modelos
 * @returns retorna un objecto de tipo order de SEQUELIZE
 */

export interface SortKeys {
  associations: any;
  search_name: string;
  key_id: string;
}

export const orderModelMulti = async (order_field: string, order_type: string, sort_keys: SortKeys[]): Promise<any> => {

  // const key_filter = sort_keys.filter((e: SortKeys) => e.key_id === order_field);
  // if (key_filter.length > 0 && order_field === key_filter[0].key_id) {
  const key_filter = sort_keys.filter((e: SortKeys) => e.search_name === order_field);
  if (key_filter.length > 0 && order_field === key_filter[0].search_name) {
    // if (key_filter.length > 0) {
    let arrayModels = []
    for await (const item of key_filter) {
      arrayModels.push(...item.associations, item.key_id, order_type)
    }
    const order = [[...arrayModels]];
    console.log('order 1', order)
    return { order };
    // }
  } else {
    const order = [[order_field, order_type]];
    console.log('order 2', order)
    return { order };
  }
};

// // ---------------------------------------------------------------------------------
// /** Example */
// const sort_keys = [
//   {
//     associations: [
//       { model: Roles, as: 'fk_rol' },
//     ],
//     search_name: 'rl_nombre',
//     key_id: 'rl_nombre',
//   },
// ]
// const order = !isEmpty(params.filters) ? await orderModelMulti(params.order_field, params.order_type, sort_keys) : {};
// // ---------------------------------------------------------------------------------