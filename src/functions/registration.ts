/************ import libraries ************/
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import to from 'await-to-js';
import { isEmpty, isNil } from 'lodash';
import Sequelize = require('sequelize');
import fs = require('fs');

/************ Import models ************/
import Users from '../models/dgUsers';
import Person from '../models/person';
import UserRoles from '../models/userRoles';
import Cities from '../models/cities';
import DocumentType from '../models/documentType';
import ReferredHistory from '../models/referredHistory';
import Passenger from '../models/passenger';
import Driver from '../models/driver';
import FeatureVehicle from '../models/featureVehicle';
import FeatureDriver from '../models/featureDriver';
import RequestDriver from '../models/requestDriver';
import Patterns from '../models/patterns';
import Marks from '../models/marks';
import Colors from '../models/colors';
import CarModels from '../models/carModels';
import ReferredWins from '../models/referredWins';

/************ import function to compare hash and tokenHeader ************/
import { compareHash, getTokenFromHeader, getHash } from './Hash';
import { orderModel } from '../functions/orderModel';

/************ Import .env ************/
const { SECRET } = process.env;

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

import { createFileS3, deleteFileS3, getFileS3 } from './uploadFile';

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function createPassenger(request: Request, response: Response, usr_id: number) {
  try {
    const { name, lastname, cellphone, gender, ref_cellphone, document, accept_dt, accept_tyc, country_code, ref_country_code } = request.body;
    // Pasajero
    const data = await Users.findByPk(request.body.usr_id);
    // ----------------------------------------------
    //guardar en la tabla de referidos
    // Verificar si ya tiene un registro de la tabla
    let responseRef = null;
    let referidoWin = null;
    let referidoUser = null;
    if (!isEmpty(ref_cellphone)) {

      // Verificar si ya el referidor tiene un refido asociado
      const refHist = await ReferredHistory.findOne({
        where: {
          rfh_referrer_id: data.getDataValue('usr_id'),
          // rfh_action: { [OP.ne]: 1 }
        }
      });
      if (refHist && refHist.getDataValue('rfh_action') != 1) {
        return response.status(500).send({ status: 'error', message: '¡El usuario ya tiene referido asociado (modo conductor)!' });
      }

      const searchUser = await Users.findOne({ where: { usr_phone_number: ref_cellphone, usr_country_code: ref_country_code,  usr_verified: 1, usr_mode: { [OP.ne]: null } } });

      if(!searchUser){
        return response.status(422).send({ status: 'error', message: 'Celular de Referido NO encontrado, verifica e intenta de nuevo.' });
      }
      
      if (refHist && refHist.getDataValue('rfh_referred_id') !== searchUser.getDataValue('usr_id')) {
        return response.status(500).send({ status: 'error', message: '¡El referido no coincide con el registado anteriormente!' });
      }

      // ¿Cuantas veces referido puede ser referido?
      // const refHist = await ReferredHistory.findOne({ where: { rfh_referred_id: searchUser.getDataValue('usr_id') } });
      // if (refHist) {
      //   return response.status(500).send({ status: 'error', message: '¡El referido ya cumplio con su maximo de redimido!' });
      // }

      if (searchUser && searchUser.getDataValue('usr_id')) {

        if (searchUser.getDataValue('usr_id') !== data.getDataValue('usr_id')) {

          // Verificar si ya existe referido creado
          const searchData = await ReferredHistory.findOne({ where: { rfh_referred_id: searchUser.getDataValue('usr_id') } });
          if (!searchData) {

            const bodyRef = {
              rfh_referred_id: searchUser.getDataValue('usr_id'), // usuario que dio a conocer la app (referido)
              rfh_referrer_id: data.getDataValue('usr_id'), // usuario nuevo de la app
              rfh_redeemed: false,
              rfh_created_at: new Date(),
              rfh_mode: searchUser.getDataValue('usr_mode'), // (1:pasajero, 2: conductor) momento en que el referido estaba en la app
              rfh_action: 1,
            };
            const [errRef, resRef] = await to(ReferredHistory.create(bodyRef));
            if (errRef) {
              return response.status(500).send({ status: 'error', message: errRef.message });
            }
            responseRef = resRef;
            // ----------------------------------------------
            // Logica para consultar tabla ReferredWins (ganancias para el referido [rfw_initial_referral_win])
            const wins = await ReferredWins.findOne({ where: { rfw_code: searchUser.getDataValue('usr_mode') == 1 ? 'PRP' : 'CRP' } });
            if (wins) {
              referidoWin = wins.getDataValue('rfw_initial_referral_win');
              referidoUser = searchUser
            }
            // ----------------------------------------------
          }
        } else {
          return response.status(422).send({ status: 'error', message: '¡El referido no puede ser su mismo numero!' });
        }

      }
    }
    // ----------------------------------------------
    const dataPerson = await Person.findOne({ where: { prs_usr_id: data.getDataValue('usr_id') } });
    if (dataPerson) {
      // Actualizar el registro
      await dataPerson.update({
        prs_name: name,
        prs_lastname: lastname,
        prs_cellphone: cellphone,
        prs_country_code: `${country_code}`,
        prs_gender: gender,
        prs_document: document,
        prs_editor_id: usr_id,
        prs_updated_at: new Date(),
      });

      const dataPassenger = await Passenger.findOne({ where: { psg_usr_id: data.getDataValue('usr_id') } });
      if (!dataPassenger) {
        const [errPass, resPass] = await to(Passenger.create({
          psg_usr_id: data.getDataValue('usr_id'),
          psg_status_id: 1, // 1: Habilitado, 2:Deshabilitado, 3:Pendiente, 4:Baneado, 5:Suspendido
          psg_created_at: new Date(),
          psg_erased: false,
          psg_step_passenger: 1,
          psg_accept_dt: accept_dt,
          psg_accept_tyc: accept_tyc
        }));
        if (errPass) {
          return response.status(500).send({ status: 'error', message: errPass.message });
        }
      }
    } else {

      const [errRol, resRol] = await to(UserRoles.create({
        usrl_usr_id: data.getDataValue('usr_id'),
        usrl_rle_id: 2, // 2:person
        usrl_created_at: new Date(),
      }));
      if (errRol) {
        if (responseRef) {
          await responseRef.destroy();
        }
        return response.status(500).send({ status: 'error', message: errRol.message });
      }

      const [errPer, resPer] = await to(Person.create({
        prs_name: name,
        prs_lastname: lastname,
        prs_cellphone: cellphone,
        prs_country_code: `${country_code}`,
        prs_gender: gender,
        prs_document: document,
        prs_cty_id: 1039, // Cali
        prs_dct_id: 1,
        prs_usr_id: data.getDataValue('usr_id'),
        prs_erased: false,
        prs_creator_id: usr_id,
        prs_created_at: new Date(),
      }));
      if (errPer) {
        if (responseRef) {
          await responseRef.destroy();
        }
        await resRol.destroy();
        return response.status(500).send({ status: 'error', message: errPer.message });
      }

      const [errPass, resPass] = await to(Passenger.create({
        psg_usr_id: data.getDataValue('usr_id'),
        psg_status_id: 1, // 1: Habilitado, 2:Deshabilitado, 3:Pendiente, 4:Baneado, 5:Suspendido
        psg_created_at: new Date(),
        psg_erased: false,
        psg_step_passenger: 1,
        psg_accept_dt: accept_dt,
        psg_accept_tyc: accept_tyc
      }));
      if (errPass) {
        if (responseRef) {
          await responseRef.destroy();
        }
        await resRol.destroy();
        await resPer.destroy();
        return response.status(500).send({ status: 'error', message: errPass.message });
      }
    }
    // ----------------------------------------------
    // Cargar ganancia al referido
    if (responseRef && referidoWin && referidoUser) {
      console.log('referidoWin', referidoWin)
      console.log('usr_mode', referidoUser.getDataValue('usr_mode'))
      if (referidoUser.getDataValue('usr_mode') == 1) {
        const dataUp = await Passenger.findOne({ where: { psg_usr_id: referidoUser.getDataValue('usr_id') } });
        await dataUp.update({ psg_discount: dataUp.getDataValue('psg_discount') + Number(referidoWin) })
      } else {
        const dataUp = await Driver.findOne({ where: { dvr_usr_id: referidoUser.getDataValue('usr_id') } });
        await dataUp.update({ dvr_discount: dataUp.getDataValue('dvr_discount') + Number(referidoWin) })
      }
    }
    // ----------------------------------------------
    await data.update({ usr_username: `${name} ${lastname}`, usr_mode: 1, usr_complete_step_passenger: true });
    response.status(200).send({ status: 'success', message: 'Ya registrado en FLAPPI! En los próximos días, te enviaremos un mensaje por whatsapp con la fecha de lanzamiento para que descargues el APP en las tiendas' });

  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function stepOneDriver(request: Request, response: Response, usr_id: number) {
  try {
    const { name, lastname, cellphone, gender, ref_cellphone, document, accept_dt, accept_tyc, country_code, ref_country_code } = request.body;
    const data = await Users.findByPk(request.body.usr_id);
    // ----------------------------------------------
    //guardar en la tabla de referidos
    // Verificar si ya tiene un registro de la tabla
    let responseRef;
    let referidoWin = null;
    let referidoUser = null;
    if (!isEmpty(ref_cellphone)) {

      // Verificar si ya el referidor tiene un refido asociado
      const refHist = await ReferredHistory.findOne({
        where: {
          rfh_referrer_id: data.getDataValue('usr_id'),
          // rfh_action: { [OP.ne]: 2 }
        }
      });
      if (refHist && refHist.getDataValue('rfh_action') != 2) {
        return response.status(500).send({ status: 'error', message: '¡El usuario ya tiene referido asociado (modo pasajero)!' });
      }

      const searchUser = await Users.findOne({ where: { usr_phone_number: ref_cellphone, usr_country_code: ref_country_code, usr_verified: 1, usr_mode: { [OP.ne]: null } } });

     if(!searchUser){
        return response.status(422).send({ status: 'error', message: 'Celular de Referido NO encontrado, verifica e intenta de nuevo.' });
      }

      if (refHist && refHist.getDataValue('rfh_referred_id') !== searchUser.getDataValue('usr_id')) {
        return response.status(500).send({ status: 'error', message: '¡El referido no coincide con el registado anteriormente!' });
      }

      // ¿Cuantas veces referido puede ser referido?
      // const refHist = await ReferredHistory.findOne({ where: { rfh_referred_id: searchUser.getDataValue('usr_id') } });
      // if (refHist) {
      //   return response.status(500).send({ status: 'error', message: '¡El referido ya cumplio con su maximo de redimido!' });
      // }

      if (searchUser && searchUser.getDataValue('usr_id')) {

        if (searchUser.getDataValue('usr_id') !== data.getDataValue('usr_id')) {

          // Verificar si ya existe referido creado
          const searchData = await ReferredHistory.findOne({ where: { rfh_referred_id: searchUser.getDataValue('usr_id') } });
          if (!searchData) {
            const bodyRef = {
              rfh_referred_id: searchUser.getDataValue('usr_id'), // usuario que dio a conocer la app (referido)
              rfh_referrer_id: data.getDataValue('usr_id'), // usuario nuevo de la app
              rfh_redeemed: false,
              rfh_created_at: new Date(),
              rfh_mode: searchUser.getDataValue('usr_mode'), // (1:pasajero, 2: conductor) momento en que el referido estaba en la app
              rfh_action: 2
            };
            const [errRef, resRef] = await to(ReferredHistory.create(bodyRef));
            if (errRef) {
              return response.status(500).send({ status: 'error', message: errRef.message });
            }
            responseRef = resRef;
            // ----------------------------------------------
            // Logica para consultar tabla ReferredWins (ganancias para el referido [rfw_initial_referral_win])
            const wins = await ReferredWins.findOne({ where: { rfw_code: searchUser.getDataValue('usr_mode') == 1 ? 'PRC' : 'CRC' } });
            if (wins) {
              referidoWin = wins.getDataValue('rfw_initial_referral_win');
              referidoUser = searchUser
            }
            // ----------------------------------------------
          }
        } else {
          return response.status(422).send({ status: 'error', message: '¡El referido no puede ser su mismo numero!' });
        }

      }
    }
    // ----------------------------------------------
    const dataPerson = await Person.findOne({ where: { prs_usr_id: data.getDataValue('usr_id') } });
    if (dataPerson) {
      // Actualizar el registro
      await dataPerson.update({
        prs_name: name,
        prs_lastname: lastname,
        prs_cellphone: cellphone,
        prs_country_code: `${country_code}`,
        prs_gender: gender,
        prs_document: document,
        prs_editor_id: usr_id,
        prs_updated_at: new Date(),
      });

      const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
      if (!dataDriver) {
        const [errDriv, resDriv] = await to(Driver.create({
          dvr_usr_id: data.getDataValue('usr_id'),
          dvr_status_id: 1, // 1:Pendiente, 2: Habilitado, 3:Deshabilitado, 4:Baneado, 5:Suspendido
          dvr_created_at: new Date(),
          dvr_erased: false,
          dvr_step_driver: 1,
          dvr_accept_dt: accept_dt,
          dvr_accept_tyc: accept_tyc
        }));
        if (errDriv) {
          return response.status(500).send({ status: 'error', message: errDriv.message });
        }
      }

      const dataRequest = await RequestDriver.findOne({ where: { rqd_usr_id: data.getDataValue('usr_id') } });
      if (!dataRequest) {
        const [errReq, respReq] = await to(RequestDriver.create({
          rqd_usr_id: data.getDataValue('usr_id'),
          rqd_created_at: new Date(),
          rqd_creator_id: usr_id,
          rqd_state: 1
        }));
        if (errReq) {
          return response.status(500).send({ status: 'error', message: errReq.message });
        }
      }
    } else {
      // Crear el registro
      const [errRol, resRol] = await to(UserRoles.create({
        usrl_usr_id: data.getDataValue('usr_id'),
        usrl_rle_id: 2, // 2:person
        usrl_created_at: new Date(),
      }));
      if (errRol) {
        if (responseRef) {
          await responseRef.destroy();
        }
        return response.status(500).send({ status: 'error', message: errRol.message });
      }

      const [errPer, resPer] = await to(Person.create({
        prs_name: name,
        prs_lastname: lastname,
        prs_cellphone: cellphone,
        prs_country_code: `${country_code}`,
        prs_gender: gender,
        prs_document: document,
        prs_dct_id: 1,
        prs_cty_id: 1039, // Cali
        prs_usr_id: data.getDataValue('usr_id'),
        prs_erased: false,
        prs_creator_id: usr_id,
        prs_created_at: new Date(),
      }));
      if (errPer) {
        if (responseRef) {
          await responseRef.destroy();
        }
        await resRol.destroy();
        return response.status(500).send({ status: 'error', message: errPer.message });
      }

      const [errDriv, resDriv] = await to(Driver.create({
        dvr_usr_id: data.getDataValue('usr_id'),
        dvr_status_id: 1, // 1:Pendiente, 2: Habilitado, 3:Deshabilitado, 4:Baneado, 5:Suspendido
        dvr_created_at: new Date(),
        dvr_erased: false,
        dvr_step_driver: 1,
        dvr_accept_dt: accept_dt,
        dvr_accept_tyc: accept_tyc
      }));
      if (errDriv) {
        if (responseRef) {
          await responseRef.destroy();
        }
        await resRol.destroy();
        await resPer.destroy();
        return response.status(500).send({ status: 'error', message: errDriv.message });
      }

      const [errReq, respReq] = await to(RequestDriver.create({
        rqd_usr_id: data.getDataValue('usr_id'),
        rqd_created_at: new Date(),
        rqd_creator_id: usr_id,
        rqd_state: 1
      }));
      if (errReq) {
        if (responseRef) {
          await responseRef.destroy();
        }
        await resRol.destroy();
        await resPer.destroy();
        await resDriv.destroy();
        return response.status(500).send({ status: 'error', message: errReq.message });
      }
    }
    // ----------------------------------------------
    // Cargar ganancia al referido
    if (responseRef && referidoWin && referidoUser) {
      console.log('referidoWin', referidoWin)
      console.log('usr_mode', referidoUser.getDataValue('usr_mode'))
      if (referidoUser.getDataValue('usr_mode') == 1) {
        const dataUp = await Passenger.findOne({ where: { psg_usr_id: referidoUser.getDataValue('usr_id') } });
        await dataUp.update({ psg_discount: dataUp.getDataValue('psg_discount') + Number(referidoWin) })
      } else {
        const dataUp = await Driver.findOne({ where: { dvr_usr_id: referidoUser.getDataValue('usr_id') } });
        await dataUp.update({ dvr_discount: dataUp.getDataValue('dvr_discount') + Number(referidoWin) })
      }
    }
    // ----------------------------------------------
    await data.update({ usr_username: `${name} ${lastname}`, usr_mode: 2 });
    response.status(200).send({ status: 'success', message: '¡Datos guardados!' });
    // ----------------------------------------------
  } catch (err) {
    console.log('err', err)
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function stepThreeDriver(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  try {
    const data = await Users.findByPk(request.body.usr_id);
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
    const dataFv = await FeatureVehicle.findOne({ where: { fv_driver_id: dataDriver.getDataValue('dvr_id') } });

    if (dataFv) {
      // Actualizar el registro
      await dataFv.update({
        ...payload,
        fv_updated_at: new Date(),
      });
    } else {
      // Crear el registro
      const [err5, res5] = await to(FeatureVehicle.create({
        ...payload,
        fv_driver_id: dataDriver.getDataValue('dvr_id'),
        fv_created_at: new Date()
      }));
      if (err5) {
        return response.status(500).send({ status: 'error', message: err5.message });
      }
    }
    response.status(200).send({ status: 'success', message: '¡Datos guardados!' });
  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function createOrUpdateDriverFileApp(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  const file = request.file;
  console.log('params =====', payload)
  console.log('file ==========', file)

  try {
    if (!file) {
      return response.status(400).send({ status: 'error', message: 'No se ha seleccionado ningún archivo.' });
    }
    // Consultar usuario
    const data = await Users.findByPk(request.body.usr_id);

    if (!data) {
      return response.status(500).send({ status: 'error', message: '¡El usuario no existe!' });
    }

    // -------------------------------------------------
    const url = `conductores/${data.getDataValue('usr_id')}`
    // -------------------------------------------------
    // Consultar Driver con id de usuario
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
    const result = await FeatureDriver.findOne({ where: { ftd_dvr_id: dataDriver.getDataValue('dvr_id'), ftd_code: { [OP.iLike]: `%${payload.ftd_code}%` } } });
    if (!isNil(result) && !isEmpty(result)) {
      console.log('(App) ¡Existe fotos...!');
      // -------------------------------------------------
      // Verificar si la foto esta en s3 
      if (result.getDataValue('ftd_value')) {
        const fotoExiste = await getFileS3(url, result.getDataValue('ftd_value'));
        // console.log('fotoExiste', fotoExiste)
        if (fotoExiste) {
          // Borrar la foto vieja del S3
          const fotoOld = result.getDataValue('ftd_value');
          await deleteFileS3(url, fotoOld);
        }
      }
      // -------------------------------------------------
      const fotoCargada = await createFileS3(file, url, payload.ftd_value);
      if (!fotoCargada) {
        return response.status(500).send({ status: 'error', message: '¡Error al subir la foto a S3!' });
      }
      // -------------------------------------------------

      if (Number(result.getDataValue('ftd_state')) === 3) {

        // Actualiza foto y estado (3:RECHAZADO) a (4:EDITADO) para revisar
        console.log('(App) ¡actualizando foto y estado...!')
        const resp = await result.update({
          ftd_value: payload.ftd_value,
          ftd_state: 4,
          ftd_updated_at: new Date(),
          ftd_editor_id: usr_id,
        });
        return response.status(200).send({ status: 'success', message: '¡Archivo actualizando y estado editado!', data: resp });

      } else {

        // actualiza foto y su estado (1:PENDIENTE) sigue igual
        console.log('(App) ¡actualizando solo foto y estado igual...!')
        const resp = await result.update({
          ftd_value: payload.ftd_value,
          ftd_updated_at: new Date(),
          ftd_editor_id: usr_id,
        });
        return response.status(200).send({ status: 'success', message: '¡Archivo actualizando!', data: resp });
      }

    } else {

      console.log('(App) ¡No existe fotos...!');
      // Codigos de FeatureType:
      // ftd_ftt_id:10 => ftd_code = foto_selfi
      // ftd_ftt_id:12 => ftd_code = foto_selfi_cedula
      // ftd_ftt_id:8 => ftd_code = cedula_frontal, cedula_atras
      // ftd_ftt_id:6 => ftd_code = licencia_frontal, licencia_atras
      // ftd_ftt_id:3 => ftd_code = vehiculo_frontal, vehiculo_atras
      // ftd_ftt_id:7 => ftd_code = tarjeta_propiedad_frontal, tarjeta_propiedad_atras
      // ftd_ftt_id:5 => ftd_code = soat

      // -------------------------------------------------
      const fotoCargada = await createFileS3(file, url, payload.ftd_value);
      if (!fotoCargada) {
        return response.status(500).send({ status: 'error', message: '¡Error al subir la foto a S3!' });
      }
      // -------------------------------------------------

      const resp = await FeatureDriver.create({
        ...payload,
        ftd_state: 1,
        ftd_dvr_id: dataDriver.getDataValue('dvr_id'),
        ftd_created_at: new Date(),
        ftd_creator_id: usr_id,
      });
      return response.status(200).send({ status: 'success', message: '¡Archivo creado!', data: resp });
    }


  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function createOrUpdateDriverFile(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  try {
    // Consultar usuario
    const data = await Users.findByPk(request.body.usr_id);
    // -------------------------------------------------
    const url = `conductores/${data.getDataValue('usr_id')}`
    // -------------------------------------------------
    // Consultar Driver con id de usuario
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
    const result = await FeatureDriver.findOne({ where: { ftd_dvr_id: dataDriver.getDataValue('dvr_id'), ftd_code: { [OP.iLike]: `%${payload.ftd_code}%` } } });
    if (!isNil(result) && !isEmpty(result)) {
      console.log('¡Existe fotos...!');

      // -------------------------------------------------
      // Verificar si la foto esta en s3 
      if (result.getDataValue('ftd_value')) {
        const fotoExiste = await getFileS3(url, result.getDataValue('ftd_value'));
        // console.log('fotoExiste', fotoExiste)
        if (fotoExiste) {
          // Borrar la foto vieja del S3
          const fotoOld = result.getDataValue('ftd_value');
          await deleteFileS3(url, fotoOld);
        }
      }
      // -------------------------------------------------

      if (Number(result.getDataValue('ftd_state')) === 3) {

        // Actualiza foto y estado (3:RECHAZADO) a (4:EDITADO) para revisar
        console.log('¡actualizando foto y estado...!')
        const resp = await result.update({
          ftd_value: payload.ftd_value,
          ftd_state: 4,
          ftd_updated_at: new Date(),
          ftd_editor_id: usr_id,
        });
        response.status(200).send({ status: 'success', message: '¡Archivo actualizando y estado editado!', data: resp });

      } else {

        // actualiza foto y su estado (1:PENDIENTE) sigue igual
        console.log('¡actualizando solo foto y estado igual...!')
        const resp = await result.update({
          ftd_value: payload.ftd_value,
          ftd_updated_at: new Date(),
          ftd_editor_id: usr_id,
        });
        response.status(200).send({ status: 'success', message: '¡Archivo actualizando!', data: resp });
      }

    } else {

      console.log('¡No existe fotos...!');
      // Codigos de FeatureType:
      // ftd_ftt_id:10 => ftd_code = foto_selfi
      // ftd_ftt_id:12 => ftd_code = foto_selfi_cedula
      // ftd_ftt_id:8 => ftd_code = cedula_frontal, cedula_atras
      // ftd_ftt_id:6 => ftd_code = licencia_frontal, licencia_atras
      // ftd_ftt_id:3 => ftd_code = vehiculo_frontal, vehiculo_atras
      // ftd_ftt_id:7 => ftd_code = tarjeta_propiedad_frontal, tarjeta_propiedad_atras
      // ftd_ftt_id:5 => ftd_code = soat

      const resp = await FeatureDriver.create({
        ...payload,
        ftd_state: 1,
        ftd_dvr_id: dataDriver.getDataValue('dvr_id'),
        ftd_created_at: new Date(),
        ftd_creator_id: usr_id,
      });
      response.status(200).send({ status: 'success', message: '¡Archivo creado!', data: resp });
    }


  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function stepChangeDriver(request: Request, response: Response, usr_id: number) {
  const payload = request.body;
  try {
    const data = await Users.findByPk(request.body.usr_id);
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
    if (!dataDriver) {
      return response.status(200).send({ status: 'success', message: `¡No se puede actualizar el paso!`, dvr_step_driver: null });
    }

    const result = await dataDriver.update({
      dvr_step_driver: payload.step_driver,
      dvr_updated_at: new Date(),
    });

    if (Number(payload.step_driver) === 5) {

      // Crear soliciutd
      const resReq = await RequestDriver.findOne({ where: { rqd_usr_id: data.getDataValue('usr_id') } });
      await resReq.update({
        rqd_state: 2,
        rqd_editor_id: usr_id,
        rqd_updated_at: new Date()
      });
      // await Email.sendEmailRequest(
      //   'info@durangoapp.com', 
      //   { name: resp.getDataValue('prs_name'), lastname: resp.getDataValue('prs_lastname'), 
      //     phone: resp.getDataValue('prs_cell'), document: resp.getDataValue('prs_document'),
      //     request: res6.getDataValue('rqd_id')
      //   }, 'editado', false
      // );
    }

    response.status(200).send({ status: 'success', message: `¡Paso ${payload.step_driver} guardado!`, dvr_step_driver: dataDriver.getDataValue('dvr_step_driver') });
  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function stepStatusDriver(request: Request, response: Response, usr_id: number) {
  try {
    const data = await Users.findByPk(request.params.id);
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: data.getDataValue('usr_id') } });
    if (!dataDriver) {
      return response.status(200).send({ status: 'success', message: `¡No se puede obtener el paso!`, dvr_step_driver: null });
    }
    response.status(200).send({ dvr_step_driver: dataDriver.getDataValue('dvr_step_driver') });
  } catch (err) {
    response.status(500).send({ status: 'error', message: err });
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function getFileById(request: Request, response: Response, usr_id: number, role_id: number) {
  try {
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: request.params.id } });
    const result = await FeatureDriver.findAll({ where: { ftd_dvr_id: dataDriver.getDataValue('dvr_id') } });
    if (!result) {
      return response.status(200).send({ status: 'success', message: `¡No se puede obtener los recursos del vehiculo!`, exist: null });
    }
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send(error);
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function getFileByStep(request: Request, response: Response, usr_id: number, role_id: number) {
  try {
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: request.params.id } });
    const params: any = request.query;

    if (!params.step || !params.step === null || params.step === '') {
      return response.status(200).send({ status: 'success', message: `¡El paso es requerido para obtener los recursos!`, exist: null });
    }

    if (params.step != 2 && params.step != 4 && params.step != 5) {
      return response.status(200).send({ status: 'success', message: `¡El paso no existe!`, exist: null });
    }

    if (params.step == 2) {
      // Codigos de FeatureType:
      // ftd_ftt_id:10 => ftd_code = foto_selfi
      // ftd_ftt_id:12 => ftd_code = foto_selfi_cedula
      // ftd_ftt_id:8 => ftd_code = cedula_frontal, cedula_atras
      // ftd_ftt_id:6 => ftd_code = licencia_frontal, licencia_atras
      const result = await FeatureDriver.findAll({ where: { ftd_dvr_id: dataDriver.getDataValue('dvr_id'), ftd_ftt_id: { [OP.in]: [10, 12, 8, 6] } } });
      if (!result) {
        return response.status(200).send({ status: 'success', message: `¡No hay recusros para este paso ${params.step}!`, exist: null });
      }
      return response.status(200).send(result);
    }

    if (params.step == 4) {
      // Codigos de FeatureType:
      // ftd_ftt_id:3 => ftd_code = vehiculo_frontal, vehiculo_atras
      const result = await FeatureDriver.findAll({ where: { ftd_dvr_id: dataDriver.getDataValue('dvr_id'), ftd_ftt_id: { [OP.in]: [3] } } });
      if (!result) {
        return response.status(200).send({ status: 'success', message: `¡No hay recusros para este paso ${params.step}!`, exist: null });
      }
      return response.status(200).send(result);
    }

    if (params.step == 5) {
      // Codigos de FeatureType:
      // ftd_ftt_id:7 => ftd_code = tarjeta_propiedad_frontal, tarjeta_propiedad_atras
      // ftd_ftt_id:5 => ftd_code = soat
      const result = await FeatureDriver.findAll({ where: { ftd_dvr_id: dataDriver.getDataValue('dvr_id'), ftd_ftt_id: { [OP.in]: [5, 7] } } });
      if (!result) {
        return response.status(200).send({ status: 'success', message: `¡No hay recusros para este paso ${params.step}!`, exist: null });
      }
      return response.status(200).send(result);
    }

  } catch (error) {
    response.status(500).send(error);
  }
}

/**
 * @author Randall Medina
 * @description Function
 * @param request
 * @param response
 */
export async function getVehicleById(request: Request, response: Response, usr_id: number, role_id: number) {
  try {
    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: request.params.id } });
    const result = await FeatureVehicle.findOne({
      where: { fv_driver_id: dataDriver.getDataValue('dvr_id') },
      attributes: { exclude: ['fv_created_at', 'fv_updated_at'] },
      include: [
        {
          model: Patterns,
          attributes: { exclude: ['pat_created_at', 'pat_updated_at'] },
          include: [
            {
              model: Marks,
              attributes: { exclude: ['mk_created_at', 'mk_updated_at'] },
            },
          ],
        },
        { model: Colors },
        { model: CarModels },
      ]
    });
    if (!result) {
      return response.status(200).send({ status: 'success', message: `¡No se puede obtener el caracteristicas del vehiculo!`, exist: null });
    }
    response.status(200).send(result);
  } catch (error) {
    console.log('error', error)
    response.status(500).send(error);
  }
}

/**
 * @author Randall Medina
 * @description Function to get by id
 * @param request
 * @param response
 */
export async function getPersonById(request: Request, response: Response) {
  try {
    const prs = await Person.findOne({
      where: { prs_usr_id: request.params.id },
      attributes: { exclude: ['prs_created_at', 'prs_creator_id', 'prs_editor_id', 'prs_updated_at'] },
      include: [
        {
          model: Users, as: 'user',
          attributes: ['usr_id', 'usr_avatar', 'usr_country_code', 'usr_phone_number', 'usr_phone_number'],
        },
        {
          model: Cities, as: 'cities',
        },
        {
          model: DocumentType, as: 'documentType',
          attributes: { exclude: ['dt_created_at', 'dt_updated_at'] },
        },
      ]
    });

    if (!prs) {
      return response.status(200).send({ status: 'success', message: `¡No se puede obtener la información personal!`, exist: null });
    }

    const dataDriver = await Driver.findOne({ where: { dvr_usr_id: prs.getDataValue('prs_usr_id') } });
    const dataPassenger = await Passenger.findOne({ where: { psg_usr_id: prs.getDataValue('prs_usr_id') } });
    response.status(200).send({
      ...prs['dataValues'],
      dvr_accept_tyc: dataDriver ? dataDriver.getDataValue('dvr_accept_tyc') : null,
      dvr_accept_dt: dataDriver ? dataDriver.getDataValue('dvr_accept_dt') : null,
      psg_accept_tyc: dataPassenger ? dataPassenger.getDataValue('psg_accept_tyc') : null,
      psg_accept_dt: dataPassenger ? dataPassenger.getDataValue('psg_accept_dt') : null
    });
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function getVehicleByType(request: Request, response: Response) {
  try {
    const order = orderModel('mk_name', 'ASC');
    const result = await Marks.findAll({
      attributes: ['mk_id', 'mk_name', 'mk_state', 'mk_type'],
      where: { mk_state: 1, mk_type: request.params.id },
      ...order,
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

/**
 * @author Randall Medina
 * @description Function to delete
 * @param request
 * @param response
 */
export async function getVehicleByPattern(request: Request, response: Response) {
  try {
    const order = orderModel('pat_name', 'ASC');
    const result = await Patterns.findAll({
      attributes: ['pat_id', 'pat_name', 'pat_state'],
      where: { pat_state: 1, pat_mk_id: request.params.id },
      ...order,
    });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send({ message: error.message });
  }
}

async function readableToBuffer(readable) {
  return new Promise((resolve) => {
    let chunks = [];

    readable.on('data', (chunk) => {
      chunks.push(chunk);
    });

    readable.on('end', () => {
      resolve(Buffer.concat(chunks));
    });
  });
}