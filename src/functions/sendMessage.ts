import to from 'await-to-js';
import * as AWS from 'aws-sdk';
import * as log4js from 'log4js';
import axios from 'axios'

// usar solo en Produccion
const sns = new AWS.SNS({
  region: 'us-east-1',
});

/** key of firebase */
const KEY_FIREBASE = 'AAAAuol2Y7Y:APA91bFlScAaGSOkJhNllSOFtWotc9jTImxQBQST4RnJtI4szICF2kMvX2tU9yvnwn_lXW9-DKb6pCnPKRNoD3gZnVds3oxqxI1xycc153J-FtrySIryp8DYkLIEWm40jg86vzhc5toY';

interface Notifications {
  body: string;
  title: string;
  sound: string;
  content_available: boolean;
}

interface BodyNotifications {
  to: string;
  notification: Notifications;
  priority: string;
}

/**
 * @author Randall Medina
 * @description Funtion to send message SMS
 * @param phone 
 * @param description 
 */
export async function sendSMS(phone: string, description: string) {
  // usar solo en Desarrollo
  // ACCESS KEY PARA ACCEDER A LOS SERVICIO DE AWS
  //-----------------------------------
  // AWS.config.region = 'us-east-1';
  // const sns = new AWS.SNS({
  //   accessKeyId: process.env.AWSAccessKeyId,
  //   secretAccessKey: process.env.AWSSecretKey,
  // });
  // -------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------
  // const payload = {
  //   Message: `COD-FLAPPI-1: ${code}`,
  //   MessageStructure: 'string',
  //   PhoneNumber: `+${phone}`,
  // };
  // const [err, result] = await to(sns.publish(payload).promise());
  // console.log(`********************** Mensaje Enviado desde AWS a: ${phone} - ${new Date()} **********************`);
  // if (err) {
  //   console.log('Error al enviar mensaje SNS - : ', err);
  //   const logger = log4js.getLogger();
  //   logger.level = 'debug';
  //   logger.debug('Error al enviar mensaje: ', err.message);
  //   logger.debug('Error al enviar mensaje1: ', err);
  //   logger.error('Error al enviar mensaje2: ', err);
  //   // return false;
  // }
  // if(result){
  //   console.log('********************** RESPONSE AWS **********************', result);
  // }
  // -------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------
  // const keys = Buffer.from('digitalvensms:Asdqwe123#').toString('base64');
  // const data = {
  //   from: 'InfoSMS',
  //   to: `+57${phone}`,
  //   text: `*CÓDIGO FLAPPI-2:*: ${code}`,
  // };
  // const header = {
  //   headers: {
  //     'Content-Type': 'application/json',
  //     Accept: 'application/json',
  //     Authorization: `Basic ${keys}`,
  //   },
  // };
  // const [err, result] = await to(axios.post('http://api.tucontactosms.com/sms/1/text/single', data, header));
  // console.log(`********************** Mensaje Enviado desde InfoBip a: ${phone} - ${new Date()} --- **********************`);
  // console.log('********************** RESPONSE INFOBIP **********************', result.data.messages);
  // if (err) {
  //   console.log('Error al enviar mensaje - :', err);
  //   // return false;
  // }
  // -------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------

  // --------------------------------------------------
  // ESTO ENVIA AL CODIGO DEL WHATSAPP DIRECTAMENTE WABOXAPP
  // --------------------------------------------------
  const payloadWb = {
    token: process.env.TOKEN_WABOXAPP,
    uid: '573161597440',
    to: `+${phone}`,
    custom_uid: new Date().getTime(),
    // text: `*CÓDIGO FLAPPI:* ${code}`,
    text: description,
  };
  const [errWp, resultWp] = await to(axios.post('https://www.waboxapp.com/api/send/chat', payloadWb));
  console.log(`********************** Mensaje Enviado desde waboxapp a: ${phone} - ${new Date()} --- **********************`);
  if (errWp) {
    console.log('Error al enviar mensaje WABOXAPP - :', errWp.message);
    // return false;
  }

  if (resultWp) {
    console.log('********************** RESPONSE WABOXAPP **********************', { statusText: resultWp.statusText, status: resultWp.status });
  }
  // -------------------------------------------------------------------------------------------
  return true;
}

export async function pushNotifications(token: string, title: string, subTitle: string, data: any) {

  const URL = 'https://fcm.googleapis.com/fcm/send';
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": `key=${process.env.KEY_FIREBASE}`,
    'apns-push-type': 'background',
    'apns-priority': '5',
    'apns-topic': 'com.react.durangoapp'
  }
  const body = {
    to: token,
    // data: {...data, type: 2},
    notification: {
      title,
      body: subTitle,
      // image: 'https://www.motor.com.co/files/article_main/files/crop/uploads/2019/07/29/5d3f4241782f2.r_1564428149670.36-120-970-587.jpeg',
      content_available: false,
      sound: 'default',
    },
    priority: 'high',
  }

  try {
    const result = await axios.post(URL, JSON.stringify(body), { headers });
    console.log('result notifications FIREBASE', result.status)
    return true;
  } catch (error) {
    console.log('******************** ERROR notifications FIREBASE', error.message);
    return null;
  }
}
