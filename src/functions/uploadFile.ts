import to from 'await-to-js';
import * as AWS from 'aws-sdk';
import * as log4js from 'log4js';
import axios from 'axios'
import fs = require('fs');


/**
 * @author Randall Medina
 * @description Funtion to create file S3
 * @param phone 
 * @param code 
 */
export async function getFileS3(myUrl: string, nameFile: string) {
  try {
    const myBucket = `${process.env.BUCKET}/public/${myUrl}`;
    const s3 = new AWS.S3();
    const payloadAWS: any = {
      Bucket: myBucket,
      Key: nameFile,
      // ACL: 'public-read',
      // ContentType: 'image/jpeg',
      // ContentEncoding: 'base64',
      // Body: buf,
      // Body: fs.createReadStream(file.path),
    };
    const getImage = await s3.getObject(payloadAWS).promise();
    // return getImage.Body.toString('utf-8');
    return getImage;
  } catch (error) {
    console.log('error getFileS3 ====', error)
    return false
  }
}

/**
 * @author Randall Medina
 * @description Funtion to create file S3
 * @param phone 
 * @param code 
 */
export async function createFileS3(file: any, myUrl: string, nameFile: string) {
  try {
    const myBucket = `${process.env.BUCKET}/public/${myUrl}`;
    const s3 = new AWS.S3();
    const payloadAWS: any = {
      Bucket: myBucket,
      Key: nameFile,
      ACL: 'public-read',
      ContentType: 'image/jpeg',
      // ContentType: 'application/pdf',
      // ContentEncoding: 'base64',
      // Body: file,
      // Body: fs.createReadStream(file),
      Body: fs.createReadStream(file.path),
    };
    const createImage = await s3.putObject(payloadAWS).promise();

    // fs.unlinkSync(file.path);

    return createImage;
  } catch (error) {
    console.log('error createFileS3 ====', error)
    return false
  }
}

/**
 * @author Randall Medina
 * @description Funtion to delete file S3
 * @param phone 
 * @param code 
 */
export async function deleteFileS3(myUrl: string, nameFile: string) {
  try {
    const myBucket = `${process.env.BUCKET}/public/${myUrl}`;
    const payloadAWS: any = {
      Bucket: myBucket,
      Key: nameFile,
      // ACL: 'public-read',
      // // ContentType: 'image/jpeg',
      // ContentType: `${file.type}`,
      // ContentEncoding: 'base64',
      // Body: file,
      // Body: buf,
      // ContentType: 'application/pdf',
      // Key: 'mykey3333.txt',
    }
    const s3 = new AWS.S3();
    const deleteImage = await s3.deleteObject(payloadAWS).promise();
    return deleteImage;
  } catch (error) {
    console.log('error deleteFileS3 ====', error)
    return false
  }
}