/************ import libraries ************/
import { isEmpty, both, is, complement, equals, isNil } from 'ramda';
import Sequelize = require('sequelize');

/************ Instance operador sequelize ************/
const OP = Sequelize.Op;

/**
 * @author Randall Medina
 * @description Function search by where
 * if receive an object to organize the variables and returns an object to filter the condition
 * @param {object} where
 */

export interface ExcludeKeys {
  key_id: string;
}

export const whereModel = async (params: any, exclude_keys: ExcludeKeys[]): Promise<any> => {
  if (!isEmpty(params)) {
      Object.keys(params).map((key: string) => {
          const mapKeys = exclude_keys.filter((e: ExcludeKeys) => e.key_id === key)
          // En esta condición verificamos si se trata de datos del mismo modelo que estamos consultando
          // Si no es el mismo modelo eliminamos la key
          if ((params[key] || params[key] === 0 ) && mapKeys.length === 0) {
              const isValidNumber = both(is(Number), complement(equals(NaN)));
              isValidNumber(params[key]) ? params[key] = { [OP.eq]: params[key] } : params[key] = { [OP.iLike]: `%${params[key]}%` }
          } else {
              delete params[key]
          }
      })
      return { [OP.and]: [params] };
  }
  return {};
}
