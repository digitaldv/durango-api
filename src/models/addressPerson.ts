import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Person from './person';
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model address_person
 */
@Table({ tableName: 'address_person', comment: '', timestamps: false })
export default class AddressPerson extends Model<AddressPerson> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public adp_id: number;

  @Comment('Nombre de la dirección, Ej: Casa, Trabajo')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public adp_name: string;

  @Comment('Punto lat, long EJ: ("Point", "POINT(23.98765 75.263587)")')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public adp_location: string;

  @Comment('Direccion: Carrera 5a #45-09')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public adp_address: string;

  @ForeignKey(() => Person)
  @Comment('FK: Id de la persona asociada')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public adp_person_id: number;

  @BelongsTo(() => Person)
  public person: Person;

  @CreatedAt
  @Comment('Fecha en la que se ha creado la dirección')
  @Column({ type: DataType.DATE, allowNull: false })
  public adp_created_at: Date;

  @UpdatedAt
  @Comment('Fecha en la que se ha editado la dirección')
  @Column({ type: DataType.DATE, allowNull: true })
  public adp_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario que crea la dirección')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public adp_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: id del usuario que edita la dirección')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public adp_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

}
