import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Questions from './questions';
import Services from './services';
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model answers
 */
@Table({ tableName: 'answers', comment: '', timestamps: false })
export default class Answers extends Model<Answers> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public ans_id: number;

  @Comment('Texto de las respuesta digitada por el usuario')
  @Column({ type: DataType.TEXT, allowNull: false })
  public ans_text: string;

  @ForeignKey(() => Questions)
  @Comment('FK: Id de la pregunta asociada')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ans_qst_id: number;

  @BelongsTo(() => Questions)
  public question: Questions;

  @CreatedAt
  @Comment('Fecha de creación de la respuesta')
  @Column({ type: DataType.DATE, allowNull: false })
  public ans_created_at: Date;

  @ForeignKey(() => Services)
  @Comment('FK: Id del servicio al que pertenece la respuesta')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ans_srv_id: number;

  @BelongsTo(() => Services)
  public services: Services;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario que digita la respuesta')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ans_usr_id: number;

  @BelongsTo(() => Users)
  public users: Users;

}
