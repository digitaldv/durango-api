import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  DataType,
  CreatedAt,
  BelongsTo,
  UpdatedAt,
  ForeignKey,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model benefits_config
 */
@Table({ tableName: 'benefits_config', comment: '', timestamps: false })
export default class BenefitsConfig extends Model<BenefitsConfig> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public bnf_id: number;

  @Comment('Tipo de usuario 1: TAXI 2: PARTICULAR 3: MOTO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public bnf_type_user: number;

  @Comment('Valor del beneficio')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public bnf_value: number;

  @Comment('Mes')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public bnf_month: number;

  @Comment('Año')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public bnf_year: number;

  @Comment('Estado: 1: ACTIVO, 2: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public bnf_state: number;

  @CreatedAt
  @Comment('Fecha de creación del beneficio')
  @Column({ type: DataType.DATE, allowNull: false })
  public bnf_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del beneficio')
  @Column({ type: DataType.DATE, allowNull: true })
  public bnf_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public bnf_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public bnf_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

}
