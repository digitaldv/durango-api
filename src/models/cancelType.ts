import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Cancellations from './cancellations';
import Users from './dgUsers';
/**
 * @author Randall Medina
 * @description Class to create model cancel_type
 */
@Table({ tableName: 'cancel_type', comment: '', timestamps: false })
export default class CancelType extends Model<CancelType> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public cnlt_id: number;

  @Comment('Nombre del tipo de cancelación')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public cnlt_name: string;

  @Comment('Descripción del tipo de cancelación')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public cnlt_description: string;

  @CreatedAt
  @Comment('Fecha de creación del beneficio')
  @Column({ type: DataType.DATE, allowNull: false })
  public cnlt_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del beneficio')
  @Column({ type: DataType.DATE, allowNull: true })
  public cnlt_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnlt_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public cnlt_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => Cancellations)
  public cancellations: Cancellations;

}
