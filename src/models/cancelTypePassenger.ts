import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import CancellationsPassenger from './cancellationsPassenger';
import Users from './dgUsers';
/**
 * @author Randall Medina
 * @description Class to create model cancel_type_passenger
 */
@Table({ tableName: 'cancel_type_passenger', comment: '', timestamps: false })
export default class CancelTypePassenger extends Model<CancelTypePassenger> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public cnltp_id: number;

  @Comment('Nombre del tipo de cancelación')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public cnltp_name: string;

  @Comment('Descripción del tipo de cancelación')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public cnltp_description: string;

  @CreatedAt
  @Comment('Fecha de creación del beneficio')
  @Column({ type: DataType.DATE, allowNull: false })
  public cnltp_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del beneficio')
  @Column({ type: DataType.DATE, allowNull: true })
  public cnltp_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnltp_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public cnltp_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => CancellationsPassenger)
  public cancellationsPassenger: CancellationsPassenger;

}
