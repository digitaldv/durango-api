import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Services from './services';
import CancelType from './cancelType';
import Driver from './driver';

/**
 * @author Randall Medina
 * @description Class to create model cancellations
 */
@Table({ tableName: 'cancellations', comment: '', timestamps: false })
export default class Cancellations extends Model<Cancellations> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public cnl_id: number;

  @Comment('Observación de la cancelación')
  @Column({ type: DataType.TEXT, allowNull: true })
  public cnl_observation: string;

  @ForeignKey(() => Services)
  @Comment('FK: Id del servicio')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnl_srv_id: number;

  @BelongsTo(() => Services)
  public services: Services;

  @ForeignKey(() => Driver)
  @Comment('FK: Id del usuario que cancela el servicio')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnl_dvr_id: number;

  @BelongsTo(() => Driver)
  public driver: Driver;

  @ForeignKey(() => CancelType)
  @Comment('id tipo de cancelacion')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnl_cnlt_id: number;

  @BelongsTo(() => CancelType)
  public cancelType: CancelType;

  @CreatedAt
  @Comment('Fecha de la creación de la cancelaión')
  @Column({ type: DataType.DATE, allowNull: false })
  public cnl_created_at: Date;

}
