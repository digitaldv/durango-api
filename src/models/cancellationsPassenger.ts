import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Services from './services';
import Passenger from './passenger';
import CancelTypePassenger from './cancelTypePassenger';

/**
 * @author Randall Medina
 * @description Class to create model cancellations_passenger
 */
@Table({ tableName: 'cancellations_passenger', comment: '', timestamps: false })
export default class CancellationsPassenger extends Model<CancellationsPassenger> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public cnlp_id: number;

  @Comment('Observación de la cancelación')
  @Column({ type: DataType.TEXT, allowNull: true })
  public cnlp_observation: string;

  @ForeignKey(() => Services)
  @Comment('FK: Id del servicio')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnlp_srv_id: number;

  @BelongsTo(() => Services)
  public services: Services;

  @ForeignKey(() => Passenger)
  @Comment('FK: Id del usuario que cancela el servicio')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnlp_psg_id: number;

  @BelongsTo(() => Passenger)
  public passenger: Passenger;

  @ForeignKey(() => CancelTypePassenger)
  @Comment('id tipo de cancelacion pasajero')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cnlp_cnltp_id: number;

  @BelongsTo(() => CancelTypePassenger)
  public cancelTypePassenger: CancelTypePassenger;

  @CreatedAt
  @Comment('Fecha de la creación de la cancelaión')
  @Column({ type: DataType.DATE, allowNull: false })
  public cnlp_created_at: Date;

}
