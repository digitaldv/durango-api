import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  PrimaryKey,
  AutoIncrement,
  CreatedAt,
  HasMany,
  Default,
} from 'sequelize-typescript';
import FeatureVehicle from './featureVehicle';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model car_models
 */
@Table({ tableName: 'car_models', comment: '', timestamps: false })
export default class CarModels extends Model<CarModels> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public crm_id: number;

  @Comment('Año: Ej: 2024')
  @Column({ type: DataType.STRING(4), allowNull: false })
  public crm_name: string;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public crm_state: number;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public crm_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public crm_created_at: Date;

  @HasMany(() => FeatureVehicle)
  public featureVehicle: FeatureVehicle[];

}
