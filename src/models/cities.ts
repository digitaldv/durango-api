import {
  Table,
  Model,
  Column,
  HasMany,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import States from './states';
import Person from './person';
import Employee from './employee';

/**
 * @author Randall Medina
 * @description Class to create model cities
 */
@Table({ tableName: 'cities', comment: '', timestamps: false })
export default class Cities extends Model<Cities> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public cty_id: number;

  @Comment('Código de la ciudad')
  @Column({ type: DataType.STRING(45), allowNull: true })
  public cty_code: string;

  @Comment('Nombre de la ciudad')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public cty_name: string;

  @ForeignKey(() => States)
  @Comment('FK: Id del departamento')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cty_dpr_id: number;

  @BelongsTo(() => States)
  public states: States;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public cty_created_at: Date;

  @HasMany(() => Person)
  public person: Person[];

  @HasMany(() => Employee)
  public employee: Employee[];

}
