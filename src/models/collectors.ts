import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Pines from './pines';

/**
 * @author Randall Medina
 * @description Class to create model collectors
 */
@Table({ tableName: 'collectors', comment: '', timestamps: false })
export default class Collectors extends Model<Collectors> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public cll_id: number;

  @Comment('Ej: Gane, Efecty, Baloto')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public cll_name: string;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public cll_state: number;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public cll_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public cll_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public cll_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registroId del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public cll_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registroId del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public cll_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => Pines)
  public pines: Pines[];

}
