import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';
import FeatureVehicle from './featureVehicle';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model colors
 */
@Table({ tableName: 'colors', comment: '', timestamps: false })
export default class Colors extends Model<Colors> {

  @AutoIncrement
  @PrimaryKey
  @Column({ type: DataType.INTEGER })
  public co_id: number;

  @Comment('Nombre del color Ej: Azul')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public co_name: string;

  @Comment('Cod Hex. Ej: #FFFFFF')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public co_color: string;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public co_state: number;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public co_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public co_created_at: Date;

  @HasMany(() => FeatureVehicle)
  public featureVehicle: FeatureVehicle[];

}
