import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  PrimaryKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import States from './states';
import Tariff from './tariff';

/**
 * @author Randall Medina
 * @description Class to create model countries
 */
@Table({ tableName: 'countries', comment: '', timestamps: false })
export default class Countries extends Model<Countries> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public cnt_id: number;

  @Comment('Código de país. Ej: COL')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public cnt_code: string;

  @Comment('Nombre del país. Ej: Colombia')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public cnt_name: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public cnt_created_at: Date;

  @HasMany(() => States)
  public states: States[];

  @HasMany(() => Tariff)
  public tariff: Tariff[];

}
