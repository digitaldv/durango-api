import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import UserRoles from './userRoles';
import Employee from './employee';

/**
 * @author Randall Medina
 * @description Class to create model dg_roles
 */
@Table({ tableName: 'dg_roles', comment: '', timestamps: false })
export default class DgRoles extends Model<DgRoles> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public rle_id: number;

  @Comment('Código del rol. Ej: CDT')
  @Column({ type: DataType.STRING(20), allowNull: false })
  public rle_code: string;

  @Comment('Nombre del rol: Ej: Conductor')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public rle_name: string;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public rle_state: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public rle_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public rle_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registroId del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public rle_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registroId del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public rle_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => UserRoles)
  public userRoles: UserRoles[];

  @HasMany(() => Employee)
  public employee: Employee[];
}
