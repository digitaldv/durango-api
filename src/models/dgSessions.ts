import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model dg_sessions
 */
@Table({ tableName: 'dg_sessions', comment: '', timestamps: false })
export default class DgSessions extends Model<DgSessions> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public dgs_id: number;

  @CreatedAt
  @Comment('Fecha de creación del token')
  @Column({ type: DataType.DATE, allowNull: false })
  public dgs_created_at: Date;

  @Comment('Valor del token')
  @Column({ type: DataType.STRING(20), allowNull: true })
  public dgs_token: string;

  @Comment('Fecha de expiración del token')
  @Column({ type: DataType.DATE, allowNull: false })
  public dgs_expiration_date: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario al que pertenece el token')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public dgs_usr_id: number;

  @BelongsTo(() => Users)
  public userToken: Users;
}
