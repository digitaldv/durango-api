import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  HasOne,
  Default,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import BenefitsConfig from './benefitsConfig';
import AddressPerson from './addressPerson';
import Answers from './answers';
import CancelType from './cancelType';
import Collectors from './collectors';
import DgRoles from './dgRoles';
import DgSessions from './dgSessions';
import DocumentType from './documentType';
import Driver from './driver';
import DriverState from './driverState';
import Faq from './faq';
import FaqCategory from './faqCategory';
import FeatureDriver from './featureDriver';
import FeaturePassenger from './featurePassenger';
import FeatureType from './featureType';
import MotivePerson from './motivePerson';
import MotiveType from './motiveType';
import Params from './params';
import Passenger from './passenger';
import Person from './person';
import Pines from './pines';
import Questions from './questions';
import ReferredHistory from './referredHistory';
import RequestDriver from './requestDriver';
import ServicesState from './servicesState';
import SmsHistory from './smsHistory';
import Tariff from './tariff';
import TariffHistory from './tariffHistory';
import UserRoles from './userRoles';
import Zones from './zone';
import Employee from './employee';

/**
 * @author Randall Medina
 * @description Class to create model dg_users
 */
@Table({ tableName: 'dg_users', comment: '', timestamps: false })
export default class Users extends Model<Users> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public usr_id: number;

  @Comment('Codigo SMS')
  @Column({ type: DataType.STRING(255), allowNull: true })
  public usr_sms_code: string;

  @Comment('Nombre de usuario')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public usr_username: string;

  @Comment('Contraseña')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public usr_password: string;

  @Comment('Modo (1:pasajero, 2: conductor)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public usr_mode: number;

  @Comment('Correo electrónico')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public usr_email: string;

  @Comment('Número de teléfono')
  @Column({ type: DataType.STRING(25), allowNull: true })
  public usr_phone_number: string;
  
  @Comment('Indicativos del pais')
  @Column({ type: DataType.STRING(5), allowNull: true })
  public usr_country_code: string;

  @Comment('URL foto de perfil')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public usr_avatar: string;

  @Comment('Ip registrada de la app donde se hace la peticion')
  @Column({ type: DataType.STRING(50), allowNull: true })
  public usr_registration_ip: string;

  @Comment('Operador del dispositivo ej: CLARO, MOVISTAR')
  @Column({ type: DataType.STRING(45), allowNull: true })
  public usr_device_operator: string;

  @Comment('Marca del dispositivo')
  @Column({ type: DataType.STRING(45), allowNull: true })
  public usr_device_brand: string;

  @Comment('Nivel del api del dispositivo (SDK)')
  @Column({ type: DataType.STRING(45), allowNull: true })
  public usr_device_api_level: string | null;

  @Comment('Version de la app')
  @Column({ type: DataType.STRING(45), allowNull: true })
  public usr_device_app_version: string;

  @Comment('IMEI del dispositivo')
  @Column({ type: DataType.STRING(200), allowNull: true })
  public usr_device_imei: string;

  @Comment('Origen de la sesion ej: APP, WEB')
  @Column({ type: DataType.STRING(45), allowNull: true })
  public usr_origin: string;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public usr_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public usr_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public usr_updated_at: Date;

  @Comment('Nivel de batería')
  @Column({ type: DataType.STRING(6), allowNull: true })
  public usr_device_battery: string;

  @Default(0)
  @Comment('Estado de sesión: 0: logout, 1: login')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public usr_state_login: number;

  @Comment('0: NO (No ha verirficado el celular), 1: SI (ya ha verirficado el celular)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public usr_verified: number;

  @Comment('Token generado con tiempo de caducidad para enviarlo por email e ingresar url')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public usr_reset_pass: string;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public usr_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public usr_editor_id: number;

  @Default(false)
  @Comment('Pasos completo del conductor y se aprobaron todos los documentos')
  @Column({ type: DataType.BOOLEAN, allowNull: true })
  public usr_complete_step_driver: boolean;

  @Default(false)
  @Comment('Pasos completo del pasajero y se aprobo')
  @Column({ type: DataType.BOOLEAN, allowNull: true })
  public usr_complete_step_passenger: boolean;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => AddressPerson)
  public addressPerson: AddressPerson[];

  @HasMany(() => Answers)
  public answers: Answers[];

  @HasMany(() => BenefitsConfig)
  public benefitsConfig: BenefitsConfig[];

  @HasMany(() => CancelType)
  public cancelType: CancelType[];

  @HasMany(() => Collectors)
  public collectors: Collectors[];

  @HasMany(() => DgRoles)
  public dgRoles: DgRoles[];

  @HasMany(() => DgSessions)
  public dgSessions: DgSessions[];

  @HasMany(() => DocumentType)
  public documentType: DocumentType[];

  @HasOne(() => Driver)
  public driver: Driver;

  @HasMany(() => DriverState)
  public driverState: DriverState[];

  @HasMany(() => Faq)
  public faq: Faq[];

  @HasMany(() => FaqCategory)
  public faqCategory: FaqCategory[];

  @HasMany(() => FeatureDriver)
  public featureDriver: FeatureDriver[];

  @HasMany(() => FeaturePassenger)
  public featurePassenger: FeaturePassenger[];

  @HasMany(() => FeatureType)
  public featureType: FeatureType[];

  @HasMany(() => MotivePerson)
  public motivePerson: MotivePerson[];

  @HasMany(() => MotiveType)
  public motiveType: MotiveType[];

  @HasMany(() => Params)
  public params: Params[];

  @HasOne(() => Passenger)
  public passenger: Passenger;

  @HasOne(() => Person)
  public person: Person;

  @HasMany(() => Pines)
  public pines: Pines[];

  @HasMany(() => Questions)
  public questions: Questions[];

  @HasMany(() => ReferredHistory)
  public referredHistory: ReferredHistory[];

  @HasMany(() => RequestDriver)
  public requestDriver: RequestDriver[];

  @HasMany(() => ServicesState)
  public servicesState: ServicesState[];

  @HasMany(() => SmsHistory)
  public smsHistory: SmsHistory[];

  @HasMany(() => Tariff)
  public tariff: Tariff[];

  @HasMany(() => TariffHistory)
  public tariffHistory: TariffHistory[];

  @HasOne(() => UserRoles)
  public userRoles: UserRoles;

  @HasMany(() => Zones)
  public zones: Zones[];

  @HasMany(() => Employee)
  public employee: Employee[];
  
}
