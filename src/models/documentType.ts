import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';

/************ import models ************/
import Person from './person';
import Users from './dgUsers';
import Employee from './employee';

/**
 * @author Randall Medina
 * @description Class to create model document_type
 */
@Table({ tableName: 'document_type', comment: '', timestamps: false })
export default class DocumentType extends Model<DocumentType> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public dt_id: number;

  @Comment('Código del documento: Ej: CC')
  @Column({ type: DataType.STRING(4), allowNull: false })
  public dt_code: string | null;

  @Comment('Descripción del tipo de documento. Ej: Cédula de ciudadanía')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public dt_description: string;

  @Comment('Estado: 0: INACTIVO 1: ACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public dt_state: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public dt_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public dt_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public dt_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public dt_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => Person)
  public person: Person[];

  @HasMany(() => Employee)
  public employee: Employee[];

}
