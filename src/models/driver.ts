import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  HasMany,
  HasOne,
  Default,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import DriverState from './driverState';
import DriverStatus from './driverStatus';
import DriverActivity from './driverActivity';
import FeatureDriver from './featureDriver';
import ScoreDriver from './scoreDriver';
import ServiceNegotiation from './serviceNegotiation';
import ServiceNegotiationHistory from './serviceNegotiationHistory';
import Services from './services';
import FeatureVehicle from './featureVehicle';

/**
 * @author Randall Medina
 * @description Class to create model driver
 */
@Table({ tableName: 'driver', comment: '', timestamps: false })
export default class Driver extends Model<Driver> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public dvr_id: number;

  @Comment('Nombre de licencia del conductor')
  @Column({ type: DataType.STRING(25), allowNull: true })
  public dvr_driving_lincense: string;

  @Comment('Contador de SERVICIOS comprados por el Conductor')
  @Default(0)
  @Column({ type: DataType.INTEGER, allowNull: true })
  public dvr_discount: number;

  @Comment('Número de viajes que ha realizado el usuario como conductor.')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public dvr_trips: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public dvr_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public dvr_updated_at: Date;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public dvr_erased: boolean;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public dvr_usr_id: number;

  @BelongsTo(() => Users)
  public users: Users;

  @ForeignKey(() => DriverState)
  @Comment('FK: Id del estado del conductor (1:disponible, 2:libre, 3:ocupado)')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public dvr_state_id: number;

  @BelongsTo(() => DriverState)
  public driverState: DriverState;

  @ForeignKey(() => DriverStatus)
  @Comment('FK: Id del status del conductor (1:Pendiente, 2: Habilitado, 3:Deshabilitado, 4:Baneado, 5:Suspendido)')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public dvr_status_id: number;

  @BelongsTo(() => DriverStatus)
  public driverStatus: DriverStatus;

  @Comment('Paso de registro conductor')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public dvr_step_driver: number;

  @Comment('Terminos y condiciones. true: Aceptado, false: No aceptado')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public dvr_accept_tyc: boolean;

  @Comment('Tratamiento de datos. true: Aceptado, false: No aceptado')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public dvr_accept_dt: boolean;

  @HasMany(() => DriverActivity)
  public driverActivity: DriverActivity[];

  @HasMany(() => FeatureDriver)
  public featureDriver: FeatureDriver[];

  @HasMany(() => ScoreDriver)
  public scoreDriver: ScoreDriver[];

  @HasMany(() => ServiceNegotiation)
  public serviceNegotiation: ServiceNegotiation[];

  @HasMany(() => ServiceNegotiationHistory)
  public serviceNegotiationHistory: ServiceNegotiationHistory[];

  @HasMany(() => Services)
  public services: Services[];

  @HasOne(() => FeatureVehicle)
  public featureVehicle: FeatureVehicle;

}
