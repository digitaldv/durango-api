import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Driver from './driver';

/**
 * @author Randall Medina
 * @description Class to create model driver_activity
 */
@Table({ tableName: 'driver_activity', comment: '', timestamps: false })
export default class DriverActivity extends Model<DriverActivity> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public dva_id: number;

  @Comment('Ubicación actual del conductor, Punto lat, long EJ: ("Point", "POINT(23.98765 75.263587)"')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public dva_current_location: string;

  @Comment('Fecha con hora del conductor')
  @Column({ type: DataType.DATE, allowNull: true })
  public dva_time: Date;

  @ForeignKey(() => Driver)
  @Comment('FK: Id del conductor ')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public dva_driver_id: number;

  @BelongsTo(() => Driver)
  public driver: Driver;

}
