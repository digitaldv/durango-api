import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import Driver from './driver';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model driver_activity_history
 */
@Table({ tableName: 'driver_activity_history', comment: '', timestamps: false })
export default class DriverActivityHistory extends Model<DriverActivityHistory> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public dvah_id: number;

  @Comment('Ubicación actual del conductor, Punto lat, long EJ: ("Point", "POINT(23.98765 75.263587)")')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public dvah_current_location: string;

  @Comment('Fecha con hora del conductor')
  @Column({ type: DataType.DATE, allowNull: true })
  public dvah_time: Date;

  @ForeignKey(() => Driver)
  @Comment('FK: Id del conductor ')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public dvah_driver_id: number;

  @BelongsTo(() => Driver)
  public driver: Driver;

}
