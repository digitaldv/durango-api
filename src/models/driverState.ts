import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  HasMany,
  Default,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Driver from './driver';

/**
 * @author Randall Medina
 * @description Class to create model driver_state
 */
@Table({ tableName: 'driver_state', comment: '', timestamps: false })
export default class DriverState extends Model<DriverState> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public ds_id: number;

  @Comment('Nombre del estado del conductor. Ej: 1:disponible, 2:libre, 3:ocupado para servicios')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public ds_state_name: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public ds_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public ds_updated_at: Date;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public ds_erased: boolean;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ds_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public ds_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => Driver)
  public driver: Driver[];

}
