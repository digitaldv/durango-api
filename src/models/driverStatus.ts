import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  HasMany,
  Default,
} from 'sequelize-typescript';
import Driver from './driver';
import Passenger from './passenger';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model driver_status
 */
@Table({ tableName: 'driver_status', comment: '', timestamps: false })
export default class DriverStatus extends Model<DriverStatus> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public dvs_id: number;

  @Comment('Nombre del tipo de estado (1:Pendiente, 2: Habilitado, 3:Deshabilitado, 4:Baneado, 5:Suspendido)')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public dvs_status_name: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public dvs_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public dvs_updated_at: Date;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public dvs_erased: boolean;

  @HasMany(() => Driver)
  public driver: Driver[];

  @HasMany(() => Passenger)
  public passenger: Passenger[];

}
