import {
    Table,
    Model,
    Column,
    Comment,
    HasMany,
    Default,
    DataType,
    BelongsTo,
    CreatedAt,
    UpdatedAt,
    PrimaryKey,
    ForeignKey,
    AutoIncrement,
    HasOne,
  } from 'sequelize-typescript';
  
  /************ import models ************/
  import Users from './dgUsers';
  import Cities from './cities';
  import DocumentType from './documentType';
  import Roles from './dgRoles';
  
  /**
   * @author Randall Medina
   * @description Class to create model employee
   */
  @Table({ tableName: 'employee', comment: '', timestamps: false })
  export default class Employee extends Model<Employee> {
  
    @AutoIncrement
    @PrimaryKey
    @Comment('Id único autoincrementable')
    @Column({ type: DataType.INTEGER })
    public emp_id: number;
  
    @ForeignKey(() => Roles)
    @Comment('FK: Id del rol')
    @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
    public emp_rle_id: number;
  
    @BelongsTo(() => Roles)
    public role: Roles;
  
    @Comment('Numero de documento')
    @Column({ type: DataType.STRING(100), allowNull: false })
    public emp_document: string;
  
    @Comment('Nombres')
    @Column({ type: DataType.STRING(100), allowNull: false })
    public emp_name: string;
  
    @Comment('Apellidos')
    @Column({ type: DataType.STRING(100), allowNull: false })
    public emp_lastname: string;
  
    @Comment('Celular')
    @Column({ type: DataType.STRING(20), allowNull: false })
    public emp_cellphone: string;
  
    @Comment('Fecha de nacimiento')
    @Column({ type: DataType.DATE, allowNull: true })
    public emp_birthdate: Date;
  
    @Comment('Género: 1: MASCULINO 2: FEMENINO')
    @Column({ type: DataType.INTEGER, allowNull: true })
    public emp_gender: number;
  
    @Default(false)
    @Comment('BORRADO LOGICO')
    @Column({ type: DataType.BOOLEAN, allowNull: false })
    public emp_erased: boolean;
  
    @CreatedAt
    @Comment('Fecha de creación del registro')
    @Column({ type: DataType.DATE, allowNull: false })
    public emp_created_at: Date;
  
    @UpdatedAt
    @Comment('Fecha de edición del registro')
    @Column({ type: DataType.DATE, allowNull: true })
    public emp_updated_at: Date;
  
    @ForeignKey(() => Users)
    @Comment('FK: Id del usuario asociado')
    @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
    public emp_usr_id: number;
  
    @BelongsTo(() => Users)
    public user: Users;
  
    @ForeignKey(() => Users)
    @Comment('FK: Id del usuario creador del registro')
    @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
    public emp_creator_id: number;
  
    @BelongsTo(() => Users)
    public creator: Users;
  
    @ForeignKey(() => Users)
    @Comment('FK: Id del usuario editor del registro')
    @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
    public emp_editor_id: number;
  
    @BelongsTo(() => Users)
    public editor: Users;
  
    @ForeignKey(() => Cities)
    @Comment('FK: Id de la ciudad')
    @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
    public emp_cty_id: number;
  
    @BelongsTo(() => Cities)
    public cities: Cities;
  
    @ForeignKey(() => DocumentType)
    @Comment('FK: Id de tipo de documento')
    @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
    public emp_dct_id: number;
  
    @BelongsTo(() => DocumentType)
    public documentType: DocumentType;
  
    @Comment('Estado: 1: ACTIVO, 2: INACTIVO')
    @Column({ type: DataType.INTEGER, allowNull: false })
    public emp_state: number;

    @Comment('dirección, Ej: Casa, Trabajo')
    @Column({ type: DataType.STRING(255), allowNull: true })
    public emp_address: string;

    @Comment('Correo electrónico')
    @Column({ type: DataType.STRING(100), allowNull: false })
    public emp_email: string;

  }
  