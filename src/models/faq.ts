import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import FaqCategory from './faqCategory';

/**
 * @author Randall Medina
 * @description Class to create model faq
 */
@Table({ tableName: 'faq', comment: '', timestamps: false })
export default class Faq extends Model<Faq> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public faq_id: number;

  @Comment('Título del FAQ')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public faq_title: string;

  @Comment('Descripción de la FAQ')
  @Column({ type: DataType.TEXT, allowNull: false })
  public faq_description: string;

  @Comment('Ordenamiento para mostrar')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public faq_order: number;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public faq_state: number;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public faq_erased: boolean;

  @Comment('Modo: 1:PASAJERO / 2: CONDUCTOR')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public faq_mode: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public faq_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public faq_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public faq_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public faq_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @ForeignKey(() => FaqCategory)
  @Comment('FK: Id de la categoría de FAQ')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public faq_fqc_id: number;

  @BelongsTo(() => FaqCategory)
  public category: FaqCategory;
}
