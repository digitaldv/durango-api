import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Faq from './faq';

/**
 * @author Randall Medina
 * @description Class to create model faq_category
 */
@Table({ tableName: 'faq_category', comment: '', timestamps: false })
export default class FaqCategory extends Model<FaqCategory> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public fqc_id: number;

  @Comment('Título de la categoría que agrupa FAQs')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public fqc_title: string;

  @Comment('Descripción de la categoría que agrupa FAQs')
  @Column({ type: DataType.TEXT, allowNull: true })
  public fqc_text: string;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public fqc_state: number;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public fqc_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public fqc_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public fqc_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public fqc_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public fqc_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => Faq)
  public faq: Faq[];

}
