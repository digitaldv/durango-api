import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  Default,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import FeatureType from './featureType';
import Users from './dgUsers';
import Driver from './driver';
import MotiveFeatureDriver from './motiveFeatureDriver';

/**
 * @author Randall Medina
 * @description Class to create model feature_driver
 */
@Table({ tableName: 'feature_driver', comment: '', timestamps: false })
export default class FeatureDriver extends Model<FeatureDriver> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public ftd_id: number;

  @Comment('Valor caracteristica')
  @Column({ type: DataType.TEXT, allowNull: false })
  public ftd_value: string;

  @Comment('Estado: 1:PENDIENTE, 2:APROBADO, 3:RECHAZADO, 4:EDITADO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public ftd_state: number;

  @Comment('Código para reconocer los campos de foto en el formulario registro')
  @Column({ type: DataType.STRING(200), allowNull: false })
  public ftd_code: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public ftd_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public ftd_updated_at: Date;

  @Comment('Comentario')
  @Column({ type: DataType.TEXT, allowNull: true })
  public ftd_comment: string;

  @ForeignKey(() => Driver)
  @Comment('FK: Id de conductor')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ftd_dvr_id: number;

  @BelongsTo(() => Driver)
  public driver: Driver;

  @ForeignKey(() => FeatureType)
  @Comment('FK: Id de característica')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ftd_ftt_id: number;

  @BelongsTo(() => FeatureType)
  public featureType: FeatureType;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ftd_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public ftd_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => MotiveFeatureDriver)
  public motiveFeatureDriver: MotiveFeatureDriver[];

}
