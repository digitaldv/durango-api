import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  Default,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import FeatureType from './featureType';
import Users from './dgUsers';
import MotiveFeaturePassenger from './motiveFeaturePassenger';
import Passenger from './passenger';

/**
 * @author Randall Medina
 * @description Class to create model feature_passenger
 */
@Table({ tableName: 'feature_passenger', comment: '', timestamps: false })
export default class FeaturePassenger extends Model<FeaturePassenger> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public ftp_id: number;

  @Comment('Valor caracteristica')
  @Column({ type: DataType.STRING(200), allowNull: false })
  public ftp_value: string;

  @Comment('Estado: 1:PENDIENTE, 2:APROBADO, 3:RECHAZADO, 4:EDITADO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public ftp_state: number;

  @Comment('ftp_code')
  @Column({ type: DataType.STRING(200), allowNull: false })
  public ftp_code: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public ftp_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public ftp_updated_at: Date;

  @Comment('Comentario')
  @Column({ type: DataType.TEXT, allowNull: true })
  public ftp_comment: string;

  @ForeignKey(() => Passenger)
  @Comment('FK: Id de pasajero')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ftp_psg_id: number;

  @BelongsTo(() => Passenger)
  public passenger: Passenger;

  @ForeignKey(() => FeatureType)
  @Comment('FK: Id de característica')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ftp_ftt_id: number;

  @BelongsTo(() => FeatureType)
  public featureType: FeatureType;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ftp_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public ftp_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => MotiveFeaturePassenger)
  public motiveFeaturePassenger: MotiveFeaturePassenger[];

}
