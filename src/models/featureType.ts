import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  Default,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import FeaturePassenger from './featurePassenger';
import FeatureDriver from './featureDriver';

/**
 * @author Randall Medina
 * @description Class to create model feature_type
 */
@Table({ tableName: 'feature_type', comment: '', timestamps: false })
export default class FeatureType extends Model<FeatureType> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public ftt_id: number;

  @Comment('Nombre de característica. Ej: Soat')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public ftt_name: string;

  @Comment('Descripción de característica')
  @Column({ type: DataType.TEXT, allowNull: false })
  public ftt_description: string;

  @Comment('Tipo de rol 1- pasajero 2- conductor')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public ftt_type_person: number;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public ftt_state: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public ftt_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public ftt_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public ftt_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public ftt_editor_id: number;

  @Comment('Cantidad de archivos')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public ftt_number_files: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => FeatureDriver)
  public featureDriver: FeatureDriver[];

  @HasMany(() => FeaturePassenger)
  public featurePassenger: FeaturePassenger[];

}
