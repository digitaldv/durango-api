import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Person from './person';
import Patterns from './patterns';
import Colors from './colors';
import CarModels from './carModels';
import Driver from './driver';

/**
 * @author Randall Medina
 * @description Class to create model feature_vehicle
 */
@Table({ tableName: 'feature_vehicle', comment: '', timestamps: false })
export default class FeatureVehicle extends Model<FeatureVehicle> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public fv_id: number;

  @Comment('umero de placa. Ej: ABC123')
  @Column({ type: DataType.STRING(10), allowNull: false })
  public fv_plaque: string;

  @Comment('Tipo de vehículo: 1: TAXI, 2: PARTICULAR, 3: MOTO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public fv_type_vehicle: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public fv_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public fv_updated_at: Date;

  @ForeignKey(() => Driver)
  @Comment('FK: Id conductor')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public fv_driver_id: number;

  @BelongsTo(() => Driver)
  public driver: Driver;

  @ForeignKey(() => Patterns)
  @Comment('FK. Id pattern')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public fv_pattern_id: number;

  @BelongsTo(() => Patterns)
  public pattern: Patterns;

  @ForeignKey(() => Colors)
  @Comment('FK: Id color')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public fv_color_id: number;

  @BelongsTo(() => Colors)
  public color: Colors;

  @ForeignKey(() => CarModels)
  @Comment('FK: Id modelo de carro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public fv_car_model_id: number;

  @BelongsTo(() => CarModels)
  public carModels: CarModels;

}
