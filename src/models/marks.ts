import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Patterns from './patterns';

/**
 * @author Randall Medina
 * @description Class to create model marks
 */
@Table({ tableName: 'marks', comment: '', timestamps: false })
export default class Marks extends Model<Marks> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public mk_id: number;

  @Comment('Nombre de la marca. EJ: Chevrolet, Kia')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public mk_name: string;

  @Comment('Estado: 1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public mk_state: number;

  @Comment('Tipo: 1: TAXI, 2: PARTICULAR')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public mk_type: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public mk_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public mk_updated_at: Date;

  @HasMany(() => Patterns)
  public patterns: Patterns[];
}
