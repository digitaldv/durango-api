import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import FeatureDriver from './featureDriver';
import MotiveType from './motiveType';

/**
 * @author Randall Medina
 * @description Class to create model motive_feature_driver
 */
@Table({ tableName: 'motive_feature_driver', comment: '', timestamps: false })
export default class MotiveFeatureDriver extends Model<MotiveFeatureDriver> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public mtvd_id: number;

  @ForeignKey(() => FeatureDriver)
  @Comment('FK: Id feature_driver')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public mtvd_ftd_id: number;

  @BelongsTo(() => FeatureDriver)
  public featureDriver: FeatureDriver;

  @ForeignKey(() => MotiveType)
  @Comment('FK: Id de motive_driver')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public mtvd_mtv_id: number;

  @BelongsTo(() => MotiveType)
  public motiveType: MotiveType;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public mtvd_created_at: Date;

}
