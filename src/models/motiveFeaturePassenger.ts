import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import FeaturePassenger from './featurePassenger';
import MotiveType from './motiveType';

/**
 * @author Randall Medina
 * @description Class to create model motive_feature_passenger
 */
@Table({ tableName: 'motive_feature_passenger', comment: '', timestamps: false })
export default class MotiveFeaturePassenger extends Model<MotiveFeaturePassenger> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public mtvf_id: number;

  @ForeignKey(() => FeaturePassenger)
  @Comment('FK: Id feature_passenger')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public mtvf_ftp_id: number;

  @BelongsTo(() => FeaturePassenger)
  public featurePassenger: FeaturePassenger;

  @ForeignKey(() => MotiveType)
  @Comment('FK: Id de motive_person')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public mtvf_mtv_id: number;

  @BelongsTo(() => MotiveType)
  public motiveType: MotiveType;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public mtvf_created_at: Date;

}
