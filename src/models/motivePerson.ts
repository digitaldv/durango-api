import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import MotiveType from './motiveType';
import Users from './dgUsers';
import Person from './person';

/**
 * @author Randall Medina
 * @description Class to create model motive_person
 */
@Table({ tableName: 'motive_person', comment: '', timestamps: false })
export default class MotivePerson extends Model<MotivePerson> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public mtvp_id: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public mtvp_created_at: Date;

  @Comment('FK: Id de motive_type')
  @ForeignKey(() => MotiveType)
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public mtvp_mtv_id: number;

  @BelongsTo(() => MotiveType)
  public motive: MotiveType;

  @ForeignKey(() => Person)
  @Comment('FK: Id de persona')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public mtvp_prs_id: number;

  @BelongsTo(() => Person)
  public person: Person;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public mtvp_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

}
