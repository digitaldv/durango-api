import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import MotivePerson from './motivePerson';
import MotiveFeatureDriver from './motiveFeatureDriver';
import MotiveFeaturePassenger from './motiveFeaturePassenger';

/**
 * @author Randall Medina
 * @description Class to create model motive_type
 */
@Table({ tableName: 'motive_type', comment: '', timestamps: false })
export default class MotiveType extends Model<MotiveType> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public mtv_id: number;

  @Comment('Nombre: Ej: Documento no legible')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public mtv_name: string;
  
  @Comment('Descripcion: Ej: El documento presenta sombras que no permiten ...')
  @Column({ type: DataType.TEXT, allowNull: false })
  public mtv_description: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public mtv_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public mtv_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public mtv_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public mtv_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => MotivePerson)
  public motivePerson: MotivePerson[];

  @HasMany(() => MotiveFeatureDriver)
  public motiveFeatureDriver: MotiveFeatureDriver[];

  @HasMany(() => MotiveFeaturePassenger)
  public motiveFeaturePassenger: MotiveFeaturePassenger[];

}
