import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Users from './dgUsers';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model params
 */
@Table({ tableName: 'params', comment: '', timestamps: false })
export default class Params extends Model<Params> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public prm_id: number;

  @Comment('Nombre del parámetro: EJ: valor_por_kilometro')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public prm_name: string;

  @Comment('Valor del parámetro. Ej: 3000')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public prm_value: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public prm_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public prm_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public prm_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public prm_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

}
