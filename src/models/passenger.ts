import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import DriverStatus from './driverStatus';
import FeatureVehicle from './featureVehicle';
import Users from './dgUsers';
import RequestService from './requestService';
import ScorePassenger from './scorePassenger';
import Services from './services';
import FeaturePassenger from './featurePassenger';

/**
 * @author Randall Medina
 * @description Class to create model passenger
 */
@Table({ tableName: 'passenger', comment: '', timestamps: false })
export default class Passenger extends Model<Passenger> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public psg_id: number;

  @Comment('Contador de DESCUENTOS ganados por el Pasajero')
  @Default(0)
  @Column({ type: DataType.INTEGER, allowNull: true })
  public psg_discount: number;

  @Comment('Número de viajes que ha realizado el usuario como pasajero.')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public psg_trips: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public psg_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public psg_updated_at: Date;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public psg_erased: boolean;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public psg_usr_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => DriverStatus)
  @Comment('FK: Id del status del pasajero (habilitado, deshabilitado, pendiente, baneado, suspendido)')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public psg_status_id: number;

  @BelongsTo(() => DriverStatus)
  public driverStatus: DriverStatus;
  
  @Comment('Paso de registro pasajero')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public psg_step_passenger: number;

  @Comment('Terminos y condiciones. true: Aceptado, false: No aceptado')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public psg_accept_tyc: boolean;

  @Comment('Tratamiento de datos. true: Aceptado, false: No aceptado')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public psg_accept_dt: boolean;

  @HasMany(() => RequestService)
  public requestService: RequestService[];

  @HasMany(() => ScorePassenger)
  public scorePassenger: ScorePassenger[];

  @HasMany(() => Services)
  public services: Services[];

  @HasMany(() => FeaturePassenger)
  public featurePassenger: FeaturePassenger[];


}
