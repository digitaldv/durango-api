import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  Default,
} from 'sequelize-typescript';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model passenger_status
 */
@Table({ tableName: 'passenger_status', comment: '', timestamps: false })
export default class PassengerStatus extends Model<PassengerStatus> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public pss_id: number;

  @Comment('Nombre del tipo de estado (habilitado, deshabilitado, baneado, suspendido, Pendiente)')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public pss_status_name: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public pss_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public pss_updated_at: Date;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public pss_erased: boolean;

}
