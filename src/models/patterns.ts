import {
  Table,
  Model,
  Column,
  Comment,
  Default,
  HasMany,
  DataType,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

/************ import models ************/
import Marks from './marks';
import FeatureVehicle from './featureVehicle';

/**
 * @author Randall Medina
 * @description Class to create model patterns
 */
@Table({ tableName: 'patterns', comment: '', timestamps: false })
export default class Patterns extends Model<Patterns> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public pat_id: number;

  @Comment('Nombre del modelo. EJ: CX-3')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public pat_name: string;

  @Comment('1: ACTIVO, 0: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public pat_state: number;
  
  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public pat_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public pat_updated_at: Date;

  @Comment('FK: Id de la marca')
  @ForeignKey(() => Marks)
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public pat_mk_id: number;

  @BelongsTo(() => Marks)
  public mark: Marks;

  @HasMany(() => FeatureVehicle)
  public featureVehicle: FeatureVehicle[];

}
