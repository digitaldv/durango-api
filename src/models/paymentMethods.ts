import {
    Table,
    Model,
    Column,
    Comment,
    HasMany,
    Default,
    DataType,
    PrimaryKey,
    AutoIncrement,
    CreatedAt,
    UpdatedAt,
    ForeignKey,
    BelongsTo,
    HasOne,
  } from 'sequelize-typescript';
  
  /************ import HasMany ************/
  import Users from './dgUsers';
import RequestService from './requestService';
import Services from './services';
  
  /**
   * @author Randall Medina
   * @description Class to create model payment_methods
   */
  @Table({ tableName: 'payment_methods', comment: 'método de pago', timestamps: false })
  export default class PaymentMethods extends Model<PaymentMethods> {
  
    @AutoIncrement
    @PrimaryKey
    @Column({ type: DataType.INTEGER })
    public pym_id: number;
   
    @Comment('Ej: Efectivo, Bancolombia, Nequi, Daviplata')
    @Column({ type: DataType.STRING(255), allowNull: false })
    public pym_name: string;

    @Comment('Estado: 1: ACTIVO, 2: INACTIVO')
    @Column({ type: DataType.INTEGER, allowNull: false })
    public pym_state: number;

    @Default(false)
    @Comment('Borrado lógico')
    @Column({ type: DataType.BOOLEAN, allowNull: false })
    public pym_erased: boolean;

    @CreatedAt
    @Comment('Fecha de creación')
    @Column({ type: DataType.DATE, allowNull: false })
    public pym_created_at: Date;
  
    @UpdatedAt
    @Comment('Fecha de edición')
    @Column({ type: DataType.DATE, allowNull: true })
    public pym_updated_at: Date;
  
    @ForeignKey(() => Users)
    @Comment('FK: Id del usuario creador')
    @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
    public pym_creator_id: number;
  
    @BelongsTo(() => Users)
    public creator: Users;
  
    @ForeignKey(() => Users)
    @Comment('FK: Id del usuario editor')
    @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
    public pym_editor_id: number;
  
    @BelongsTo(() => Users)
    public editor: Users;

    @HasMany(() => RequestService)
    public requestService: RequestService[];

    @HasMany(() => Services)
    public services: Services[];
  
  }
  