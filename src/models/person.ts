import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  Default,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  HasOne,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Cities from './cities';
import DocumentType from './documentType';
import AddressPerson from './addressPerson';
import MotivePerson from './motivePerson';

/**
 * @author Randall Medina
 * @description Class to create model person
 */
@Table({ tableName: 'person', comment: 'Conductor o Pasajero', timestamps: false })
export default class Person extends Model<Person> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public prs_id: number;

  @Comment('Numero de documento')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public prs_document: string;

  @Comment('Nombres')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public prs_name: string;

  @Comment('Apellidos')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public prs_lastname: string;

  @Comment('Celular')
  @Column({ type: DataType.STRING(20), allowNull: false })
  public prs_cellphone: string;

  @Comment('Indicativos del pais')
  @Column({ type: DataType.STRING(5), allowNull: true })
  public prs_country_code: string;

  @Comment('Fecha de nacimiento')
  @Column({ type: DataType.DATE, allowNull: true })
  public prs_birthdate: Date;

  @Comment('Género: 1: MASCULINO 2: FEMENINO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public prs_gender: number;

  @Comment('PIN de seguridad')
  @Column({ type: DataType.STRING(6), allowNull: true })
  public prs_pin: string;

  @Comment('Correo electrónico')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public prs_email: string;

  @Default(false)
  @Comment('BORRADO LOGICO')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public prs_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public prs_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public prs_updated_at: Date;

  @Comment('Punto lat, long EJ: ("Point", "POINT(23.98765 75.263587)")')
  @Column({ type: DataType.GEOMETRY, allowNull: true })
  public prs_location: string;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario asociado')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public prs_usr_id: number;

  @BelongsTo(() => Users)
  public user: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public prs_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public prs_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @ForeignKey(() => Cities)
  @Comment('FK: Id de la ciudad')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public prs_cty_id: number;

  @BelongsTo(() => Cities)
  public cities: Cities;

  @ForeignKey(() => DocumentType)
  @Comment('FK: Id de tipo de documento')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public prs_dct_id: number;

  @BelongsTo(() => DocumentType)
  public documentType: DocumentType;

  @HasMany(() => AddressPerson)
  public addressPerson: AddressPerson[];

  @HasMany(() => MotivePerson)
  public motivePerson: MotivePerson[];

}
