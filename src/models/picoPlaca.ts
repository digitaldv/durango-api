import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  PrimaryKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model pico_placa
 */
@Table({ tableName: 'pico_placa', comment: '', timestamps: false })
export default class PicoPlaca extends Model<PicoPlaca> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public pyp_id: number;

  @Comment('Nombre del día: Ej: lunes')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public pyp_day: number;

  @Comment('Número de placa afectado. Ej: 0')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public pyp_plaque_number: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public pyp_created_at: Date;

}
