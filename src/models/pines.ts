import {
  Table,
  Model,
  Column,
  Default,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Collectors from './collectors';

/**
 * @author Daniela Romero
 * @description Class to create model pines
 */
@Table({ tableName: 'pines', comment: '', timestamps: false })
export default class Pines extends Model<Pines> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public pn_id: number;

  @Comment('Codigo del pin ej: A3R53W')
  @Column({ type: DataType.STRING(6), allowNull: false })
  public pn_cod_pin: string;

  @Comment('Valor del pin. Ej: $50000')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public pn_value: number;

  @Comment('Estado; 1: Pendiente, 2: Vendido')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public pn_state: number;

  @Comment('Numero de servicios. Ej: 1, 2, 4, ...')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public pn_number_services: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public pn_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public pn_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del conductor que redimió el pin')
  @Column({ type: DataType.INTEGER, allowNull: true,  onDelete: 'NO ACTION' })
  public pn_driver_id: number;

  @BelongsTo(() => Users, { foreignKey: 'pn_driver_id' })
  public driver: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public pn_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;
  
  @ForeignKey(() => Collectors)
  @Comment('FK: Id del collector')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public pn_collector_id: number;

  @BelongsTo(() => Collectors)
  public collectors: Collectors;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public pn_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

}
