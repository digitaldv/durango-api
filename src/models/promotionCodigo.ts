import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
  Default,
  CreatedAt,
  UpdatedAt
} from 'sequelize-typescript';

/************ import HasMany ************/
import Users from './dgUsers';

/**
 * @author Styk Medina
 * @description Class to create model PromotionCode
 */
@Table({ tableName: 'promotion_code', comment: 'Codigos de descuento x Tipo de campaña - Finaliza cuando se acaba el saldo o la fecha final', timestamps: false })
export default class PromotionCode extends Model<PromotionCode> {

  @AutoIncrement
  @PrimaryKey
  @Column({ type: DataType.INTEGER })
  public prcod_id: number;

  @Comment('1: Influencer, 2: Empresa, 3: Kurati')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public prcod_type: number;

  @Comment('Nombre del Embajador, influencer, empresa, etc.... originador del codigo')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public prcod_originator: string;

  @Comment('Mindo2022')
  @Column({ type: DataType.STRING(20), allowNull: false })
  public prcod_code: string;

  @Comment('Total de redimidos (se van restando)')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public prcod_total: number;

  @Comment('Estado: 1: ACTIVO, 2: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public prcod_state: number;

  @Comment('Máximo de códigos que se pueden redimir')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public prcod_total_max: number;

  @Comment('1: desc pesos, 2: desc porcentaje')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public prcod_discount_type: number;

  @Comment('Porcentaje o pesos de descuento')
  @Column({ type: DataType.FLOAT, allowNull: true })
  public prcod_discount: number;

  @Comment('Vigencia - Fecha de inicio')
  @Column({ type: DataType.DATEONLY, allowNull: true })
  public prcod_start_date: Date;

  @Comment('Vigencia - Fecha Fin')
  @Column({ type: DataType.DATEONLY, allowNull: true })
  public prcod_end_date: Date;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: true })
  public prcod_erased: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public prcod_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public prcod_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('Creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public prcod_creator_id: number;

  @BelongsTo(() => Users, { foreignKey: 'prcod_creator_id' })
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('Editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public prcod_editor_id: number;

  @BelongsTo(() => Users, { foreignKey: 'prcod_editor_id' })
  public editor: Users;

}
