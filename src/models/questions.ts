import {
  Table,
  Model,
  Column,
  Comment,
  HasMany,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  Default,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Answers from './answers';
import ScoreDriver from './scoreDriver';
import ScorePassenger from './scorePassenger';

/**
 * @author Randall Medina
 * @description Class to create model questions
 */
@Table({ tableName: 'questions', comment: '', timestamps: false })
export default class Questions extends Model<Questions> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public qst_id: number;

  @Comment('Título de la pregunta')
  @Column({ type: DataType.STRING(50), allowNull: false })
  public qst_title: string;

  @Comment('Texto de la pregunta')
  @Column({ type: DataType.TEXT, allowNull: true })
  public qst_text: string;

  @Comment('Dice el tipo de pregunta (Rango o pregunta abierta)')
  @Column({ type: DataType.STRING(10), allowNull: false })
  public qst_type: string;

  @Comment('Modo (1:pasajero, 2: conductor)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public qst_mode: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public qst_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public qst_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public qst_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public qst_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => Answers)
  public answers: Answers[];

  @HasMany(() => ScoreDriver)
  public scoreDriver: ScoreDriver[];

  @HasMany(() => ScorePassenger)
  public scorePassenger: ScorePassenger[];

}
