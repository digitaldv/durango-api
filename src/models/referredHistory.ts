import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model referred_history
 */
@Table({ tableName: 'referred_history', comment: '', timestamps: false })
export default class ReferredHistory extends Model<ReferredHistory> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public rfh_id: number;

  @Comment('Redimido: true o false')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public rfh_redeemed: boolean;

  @Comment('Modo en que el referido tenia seleccionado en la app (1:pasajero, 2: conductor)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rfh_mode: number;

  @Comment('Accion que el referidor se esta registrando en la app por primera vez como (1: pasajero, 2: conductor)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rfh_action: number;

  @Comment('fecha de expiracion')
  @Column({ type: DataType.DATE, allowNull: true })
  public rfh_expiration: Date;
  
  @ForeignKey(() => Users)
  @Comment('Id referido (usuario que dio a conocer la app)')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public rfh_referred_id: number;

  @BelongsTo(() => Users, { foreignKey: 'rfh_referred_id' })
  public referred: Users;

  @ForeignKey(() => Users)
  @Comment('Id referidor (usuario nuevo de la app)')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public rfh_referrer_id: number;

  @BelongsTo(() => Users, { foreignKey: 'rfh_referrer_id' })
  public referrer: Users;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public rfh_created_at: Date;

}
