import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/

/**
 * @author Randall Medina
 * @description Class to create model referred_wins
 */
@Table({ tableName: 'referred_wins', comment: '', timestamps: false })
export default class ReferredWins extends Model<ReferredWins> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public rfw_id: number;

  @Comment('PRP (Pasajero refiere pasajero), PRC (Pasajero refiere conductor), CRP (Conductor refiere pasajero), CRC (Conductor refiere conductor)')
  @Column({ type: DataType.STRING(20), allowNull: false })
  public rfw_code: string;

  @Comment('Ganancia referido inicial')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public rfw_initial_referral_win: number;

  @Comment('Ganancia referidor final')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public rfw_final_referral_win: number;

  @Comment('Estado: 1: ACTIVO, 2: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public rfw_state: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public rfw_created_at: Date;

}
