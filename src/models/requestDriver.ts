import {
  Table,
  Model,
  Column,
  Default,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Person from './person';

/**
 * @author Randall Medina
 * @description Class to create model request_driver
 */
@Table({ tableName: 'request_driver', comment: '', timestamps: false })
export default class RequestDriver extends Model<RequestDriver> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public rqd_id: number;

  @Comment('1-INCOMPLETA, 2-PENDIENTE, 3-APROBADO, 4-RECHAZADO, 5-POR REVISAR')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public rqd_state: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public rqd_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public rqd_updated_at: Date;

  @Comment('Comentario de la solicitud')
  @Column({ type: DataType.TEXT, allowNull: true })
  public rqd_comment: string;

  @Comment('Fecha de aprobación o rechazo de solicitud')
  @Column({ type: DataType.DATE, allowNull: true })
  public rqd_date_approve_reject: Date;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: true })
  public rqd_erased: boolean;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public rqd_usr_id: number;

  @BelongsTo(() => Users, { foreignKey: 'rqd_usr_id' })
  public user: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario que aprobó')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public rqd_user_approve_id: number;

  @BelongsTo(() => Users, { foreignKey: 'rqd_user_approve_id' })
  public user_approve: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public rqd_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public rqd_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

}
