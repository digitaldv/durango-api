import {
  Table,
  Model,
  Column,
  Default,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  HasMany,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Person from './person';
import Passenger from './passenger';
import RequestServiceHistory from './requestServiceHistory';
import ServiceNegotiation from './serviceNegotiation';
import ServiceNegotiationHistory from './serviceNegotiationHistory';
import PaymentMethods from './paymentMethods';

/**
 * @author Randall Medina
 * @description Class to create model request_service
 */
@Table({ tableName: 'request_service', comment: '', timestamps: false })
export default class RequestService extends Model<RequestService> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public rqs_id: number;

  @Comment('Ubicación inicial del viaje del pasajero')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public rqs_initial_location: string;

  @Comment('Ubicación final del viaje del pasajero')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public rqs_destination: string;

  @Comment('Precio del servicio sugerido por la plataforma')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rqs_suggested_price: number;

  @Comment('Precio digitado por el usuario (No será menor al que la plataforma sugiere)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rqs_passenger_price: number;

  @Comment('Fecha en la que se hace la petición del servicio')
  @Column({ type: DataType.DATE, allowNull: true })
  public rqs_offer_date: Date;

  @Comment('Fecha con hora inicial de creación de la petición (se actializa cuando el rango cambia)')
  @Column({ type: DataType.DATE, allowNull: true })
  public rqs_range_date: Date;

  @Comment('Rango en el que se amplia la busqueda de conductores (cada 10 segundo se actualiza)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rqs_range_passenger: number;

  @Comment('Estado de la petición del servicio - 0: Oferta cancelada por el pasajero, 1: En oferta, 2: Aceptada,  3: Reemplazado por nuevo precio, 4: Cancelado por inactividad')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rqs_status: number;

  @ForeignKey(() => Passenger)
  @Comment('FK: Id del pasajero')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public rqs_passenger_id: number;

  @BelongsTo(() => Passenger, { foreignKey: 'rqs_passenger_id' })
  public passenger: Passenger;

  @Comment('true si el servicio se está creando con un descuento que tenga el pasajero, en caso contrario, false')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public rqs_withdiscount: boolean;

  @Comment('true si el servicio se está creando con el precio preferencial para recogida en menos de 5 minutos por parte del pasajero, en caso contrario, false')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public rqs_withextraprice: boolean;

  @ForeignKey(() => PaymentMethods)
  @Comment('FK: Id del método de pago utilizado por el pasajero')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public rqs_payment_method_id: number;

  @BelongsTo(() => Users, { foreignKey: 'rqs_payment_method_id' })
  public paymentMethod: Users;

  @HasMany(() => RequestServiceHistory)
  public requestServiceHistory: RequestServiceHistory[];

  @HasMany(() => ServiceNegotiation)
  public serviceNegotiation: ServiceNegotiation[];

  @HasMany(() => ServiceNegotiationHistory)
  public serviceNegotiationHistory: ServiceNegotiationHistory[];

}
