import {
  Table,
  Model,
  Column,
  Default,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import RequestService from './requestService';

/**
 * @author Randall Medina
 * @description Class to create model request_service_history
 */
@Table({ tableName: 'request_service_history', comment: '', timestamps: false })
export default class RequestServiceHistory extends Model<RequestServiceHistory> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public rqsh_id: number;

  @Comment('Ubicación inicial del viaje del pasajero')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public rqsh_initial_location: string;

  @Comment('Ubicación final del viaje del pasajero')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public rqsh_destination: string;

  @Comment('Precio digitado por el usuario (No será menor al que la plataforma sugiere)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rqsh_passenger_price: number;

  @Comment('Precio del servicio sugerido por la plataforma')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rqsh_suggested_price: number;

  @Comment('Fecha en la que se hace la petición del servicio')
  @Column({ type: DataType.DATE, allowNull: true })
  public rqsh_offer_date: Date;

  @Comment('Fecha con hora inicial de creación de la petición (se actializa cuando el rango cambia)')
  @Column({ type: DataType.DATE, allowNull: true })
  public rqsh_range_date: Date;

  @Comment('Rango en el que se amplia la busqueda de conductores (cada 10 segundo se actualiza)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public rqsh_range_passenger: number;

  @ForeignKey(() => RequestService)
  @Comment('FK: Id del request service')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public rqsh_request_id: number;

  @BelongsTo(() => RequestService, { foreignKey: 'rqsh_request_id' })
  public requestService: RequestService;

}
