import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  ForeignKey,
  PrimaryKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Services from './services';
import Questions from './questions';
import Driver from './driver';

/**
 * @author Randall Medina
 * @description Class to create model score_driver
 */
@Table({ tableName: 'score_driver', comment: '', timestamps: false })
export default class ScoreDriver extends Model<ScoreDriver> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public scd_id: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public scd_created_at: Date;

  @Comment('Valor de calificación: 1-5')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public scd_value: number;

  @ForeignKey(() => Questions)
  @Comment('FK: Id de la pregunta contestada')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public scd_qst_id: number;

  @BelongsTo(() => Questions)
  public questions: Questions;

  @ForeignKey(() => Services)
  @Comment('FK: Id del servicio calificado')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public scd_srv_id: number;

  @BelongsTo(() => Services)
  public services: Services;

  @ForeignKey(() => Driver)
  @Comment('FK: Id del conductor calificado')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public scd_dvr_id: number;

  @BelongsTo(() => Driver)
  public driver: Driver;

}
