import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  ForeignKey,
  PrimaryKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Services from './services';
import Questions from './questions';
import Passenger from './passenger';

/**
 * @author Randall Medina
 * @description Class to create model score_passenger
 */
@Table({ tableName: 'score_passenger', comment: '', timestamps: false })
export default class ScorePassenger extends Model<ScorePassenger> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public scp_id: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public scp_created_at: Date;

  @Comment('Valor de calificación: 1-5')
  @Column({ type: DataType.FLOAT, allowNull: false })
  public scp_value: number;

  @ForeignKey(() => Questions)
  @Comment('FK: Id de la pregunta contestada')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public scp_qst_id: number;

  @BelongsTo(() => Questions)
  public questions: Questions;

  @ForeignKey(() => Services)
  @Comment('FK: Id del servicio calificado')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public scp_srv_id: number;

  @BelongsTo(() => Services)
  public services: Services;

  @ForeignKey(() => Passenger)
  @Comment('FK: Id del pasajero calificado')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public scp_psg_id: number;

  @BelongsTo(() => Passenger)
  public passenger: Passenger;

}
