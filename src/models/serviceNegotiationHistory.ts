import {
  Table,
  Model,
  Column,
  Default,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Person from './person';
import Passenger from './passenger';
import RequestService from './requestService';
import Driver from './driver';

/**
 * @author Randall Medina
 * @description Class to create model service_negotiation_history
 */
@Table({ tableName: 'service_negotiation_history', comment: '', timestamps: false })
export default class ServiceNegotiationHistory extends Model<ServiceNegotiationHistory> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public srnh_id: number;

  @Comment('Precio ofrecido por el conductor')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public srnh_driver_price: number;

  @Comment('Precio ofrecido por el pasajero')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public srnh_passenger_price: number;

  @Comment('Fecha de con hora para la vigencia de la oferta')
  @Column({ type: DataType.DATE, allowNull: true })
  public srnh_offer_date: Date;

  @ForeignKey(() => RequestService)
  @Comment('FK: Id del request service')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public srnh_rqs_id: number;

  @BelongsTo(() => RequestService, { foreignKey: 'srnh_rqs_id' })
  public requestService: RequestService;

  @ForeignKey(() => Driver)
  @Comment('FK: Id del conductor')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public srnh_driver_id: number;

  @BelongsTo(() => Driver, { foreignKey: 'srnh_driver_id' })
  public driver: Driver;


}
