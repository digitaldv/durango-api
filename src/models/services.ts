import {
  Table,
  Model,
  Column,
  Comment,
  HasOne,
  Default,
  HasMany,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Cancellations from './cancellations';
import Passenger from './passenger';
import Driver from './driver';
import ServicesState from './servicesState';
import Answers from './answers';
import ScoreDriver from './scoreDriver';
import ScorePassenger from './scorePassenger';
import PaymentMethods from './paymentMethods';

/**
 * @author Randall Medina
 * @description Class to create model services
 */
@Table({ tableName: 'services', comment: '', timestamps: false })
export default class Services extends Model<Services> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public srv_id: number;

  @Comment('Tarifa')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public srv_tariff: number;

  @Comment('Dirección de inicio del servicio')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public srv_start_address: string;

  @Comment('Punto de inicio lat, long EJ: ("Point", "POINT(23.98765 75.263587)")')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public srv_start_location: string;

  @Comment('Dirección de destino del servicio')
  @Column({ type: DataType.STRING(255), allowNull: false })
  public srv_dest_address: string;

  @Comment('Punto de destino lat, long EJ: ("Point", "POINT(23.98765 75.263587)")')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public srv_dest_location: string;

  @Comment('Dirección final del servicio')
  @Column({ type: DataType.STRING(255), allowNull: true })
  public srv_end_address: string;

  @Comment('Punto final lat, long EJ: ("Point", "POINT(23.98765 75.263587)")')
  @Column({ type: DataType.GEOMETRY, allowNull: true })
  public srv_end_location: string;

  @Comment('Observación del pasajero')
  @Column({ type: DataType.TEXT, allowNull: true })
  public srv_observation_passenger: string;

  @Comment('observation conductor')
  @Column({ type: DataType.TEXT, allowNull: true })
  public srv_observation_driver: string;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public srv_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public srv_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public srv_updated_at: Date;

  @Comment('Código de seguridad')
  @Column({ type: DataType.STRING(4), allowNull: true })
  public srv_code: string;

  @Comment('Descuento del servicio')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public srv_discount: number;

  @Comment('Precio sugerido')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public srv_suggested_price: number;

  @Comment('Precio ofrecido por el pasajero')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public srv_passenger_price: number;

  @Comment('Precio ofrecido por el conductor')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public srv_driver_price: number;

  @ForeignKey(() => Passenger)
  @Comment('id del pasajero')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public srv_passenger_id: number;

  @BelongsTo(() => Passenger, { foreignKey: 'srv_passenger_id' })
  public passenger: Passenger;

  @ForeignKey(() => Driver)
  @Comment('FK: Id del usuario conductor')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public srv_driver_id: number;

  @BelongsTo(() => Driver, { foreignKey: 'srv_driver_id' })
  public driver: Driver;

  @ForeignKey(() => ServicesState)
  @Comment('FK: Id del estado del servicio')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public srv_state_id: number;

  @BelongsTo(() => ServicesState, { foreignKey: 'srv_state_id' })
  public servicesState: ServicesState;

  @ForeignKey(() => PaymentMethods)
  @Comment('id del metodo pago')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public srv_payment_method_id: number;

  @BelongsTo(() => PaymentMethods, { foreignKey: 'srv_payment_method_id' })
  public payment_method: PaymentMethods;

  @Comment('true si el servicio se está creando con un descuento que tenga el pasajero, en caso contrario, false')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public srv_withdiscount: boolean;

  @Comment('true si el servicio se está creando con el precio preferencial para recogida en menos de 5 minutos por parte del pasajero, en caso contrario, false')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public srv_withextraprice: boolean;

  @HasMany(() => Answers)
  public answers: Answers[];

  @HasMany(() => Cancellations)
  public cancellations: Cancellations[];

  @HasMany(() => ScoreDriver)
  public scoreDriver: ScoreDriver[];

  @HasMany(() => ScorePassenger)
  public scorePassenger: ScorePassenger[];

}
