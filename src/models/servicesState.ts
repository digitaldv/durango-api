import {
  Table,
  Model,
  Column,
  HasMany,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
  UpdatedAt,
  Default,
} from 'sequelize-typescript';

/************ import models ************/
import Cities from './cities';
import Countries from './countries';
import Users from './dgUsers';
import Services from './services';

/**
 * @author Randall Medina
 * @description Class to create model services_state
 */
@Table({ tableName: 'services_state', comment: '', timestamps: false })
export default class ServicesState extends Model<ServicesState> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public svs_id: number;

  @Comment('Nombre del estado del servicio. Ej: 1:SOLICITADO, 2:EN CAMINO, 3:LLEGO CONDUCTOR, 4:VIAJANDO, 5:FINALIZADO, 6:CANCELADO CONDUCTOR, 7:CANCELADO PASAJERO, 8:CANCELADO SISTEMA')
  @Column({ type: DataType.STRING(100), allowNull: true })
  public svs_state_name: string;

  @Default(false)
  @Comment('Borrado lógico')
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  public svs_erased: boolean;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public svs_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public svs_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public svs_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public svs_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @HasMany(() => Services)
  public services: Services[];
}
