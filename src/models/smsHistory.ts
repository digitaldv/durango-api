import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model sms_history
 */
@Table({ tableName: 'sms_history', comment: '', timestamps: false })
export default class SmsHistory extends Model<SmsHistory> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public smsh_id: number;

  @Comment('Codigo SMS')
  @Column({ type: DataType.STRING(6), allowNull: false })
  public smsh_code: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public smsh_created_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario que recibió el SMS')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public smsh_usr_id: number;

  @BelongsTo(() => Users)
  public user: Users;

}
