import {
  Table,
  Model,
  Column,
  HasMany,
  Comment,
  DataType,
  BelongsTo,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Cities from './cities';
import Countries from './countries';

/**
 * @author Randall Medina
 * @description Class to create model states
 */
@Table({ tableName: 'states', comment: '', timestamps: false })
export default class States extends Model<States> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public dpr_id: number;

  @Comment('Código del departamento. Ej: VAL')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public dpr_code: string;

  @Comment('Nombre del departamento. Ej: Valle del Cauca')
  @Column({ type: DataType.STRING(45), allowNull: false })
  public dpr_name: string;

  @ForeignKey(() => Countries)
  @Comment('FK: Id del país')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public dpr_cnt_id: number;

  @BelongsTo(() => Countries)
  public countries: Countries;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public dpr_created_at: Date;

  @HasMany(() => Cities)
  public cities: Cities[];

}
