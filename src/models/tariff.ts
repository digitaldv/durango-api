import {
  Table,
  Model,
  Column,
  Default,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';
import Countries from './countries';

/**
 * @author Randall Medina
 * @description Class to create model tariff
 */
@Table({ tableName: 'tariff', comment: 'tarifa', timestamps: true })
export default class Tariff extends Model<Tariff> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public trf_id: number;

  @Comment('Valor tarifa mínima')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public trf_value: number;

  @Comment('Tipo de vehículo. 1: TAXI, 2: PARTICULAR, 3: MOTO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public trf_vehicle_type: number;

  @Comment('Estado: 1: ACTIVO, 2: INACTIVO')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public trf_state: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public trf_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public trf_updated_at: Date;

  @Comment('Año en funcionamiento')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public trf_year: number;

  @Comment('Codigo de la moneda (EUR, COP)')
  @Column({ type: DataType.STRING(11), allowNull: false })
  public trf_money_code: string;

  @Comment('Porcentaje de impuesto segun el pais (IVA)')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public trf_tax: number;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public trf_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public trf_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

  @ForeignKey(() => Countries)
  @Comment('FK: Id del país')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public trf_country_id: number;

  @BelongsTo(() => Countries)
  public countries: Countries;

}
