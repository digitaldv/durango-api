import {
  Table,
  Model,
  Column,
  Comment,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model tariff_history
 */
@Table({ tableName: 'tariff_history', comment: '', timestamps: false })
export default class TariffHistory extends Model<TariffHistory> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public trfh_id: number;

  @Comment('Valor anterior de la tarifa')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public trfh_previous_value: number;

  @Comment('Valor actualizado de la tarifa')
  @Column({ type: DataType.INTEGER, allowNull: false })
  public trfh_value: number;

  @Comment('Tipo de vehículo: 1: TAXI, 2: PARTICULAR, 3: MOTO')
  @Column({ type: DataType.INTEGER, allowNull: true })
  public trfh_vehicule_type: number;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public trfh_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public trhf_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public trfh_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public trfh_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

}
