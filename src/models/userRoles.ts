import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Roles from './dgRoles';
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model user_roles
 */
@Table({ tableName: 'user_roles', comment: '', timestamps: false })
export default class UserRoles extends Model<UserRoles> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public usrl_id: number;

  @ForeignKey(() => Roles)
  @Comment('FK: Id del rol (1:SUPERADMIN, 2:PERSON [conductor/pasajero]), 3: ...')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public usrl_rle_id: number;

  @BelongsTo(() => Roles)
  public role: Roles;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public usrl_usr_id: number;

  @BelongsTo(() => Users)
  public user: Users;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public usrl_created_at: Date;

}
