import {
  Table,
  Model,
  AutoIncrement,
  PrimaryKey,
  Column,
  DataType,
  Comment,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

/************ import models ************/
import Users from './dgUsers';

/**
 * @author Randall Medina
 * @description Class to create model zones
 */
@Table({ tableName: 'zones', comment: '', timestamps: false })
export default class Zones extends Model<Zones> {

  @AutoIncrement
  @PrimaryKey
  @Comment('Id único autoincrementable')
  @Column({ type: DataType.INTEGER })
  public zn_id: number;

  @Comment('Poligono trazado entre diferentes puntos')
  @Column({ type: DataType.GEOMETRY, allowNull: false })
  public zn_coordinates: string;

  @Comment('Nombre de la zona')
  @Column({ type: DataType.STRING(100), allowNull: false })
  public zn_name: string;

  @Comment('Descripción de la zona')
  @Column({ type: DataType.TEXT, allowNull: true })
  public zn_description: string;

  @CreatedAt
  @Comment('Fecha de creación del registro')
  @Column({ type: DataType.DATE, allowNull: false })
  public zn_created_at: Date;

  @UpdatedAt
  @Comment('Fecha de edición del registro')
  @Column({ type: DataType.DATE, allowNull: true })
  public zn_updated_at: Date;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario creador del registro')
  @Column({ type: DataType.INTEGER, allowNull: false, onDelete: 'NO ACTION' })
  public zn_creator_id: number;

  @BelongsTo(() => Users)
  public creator: Users;

  @ForeignKey(() => Users)
  @Comment('FK: Id del usuario editor del registro')
  @Column({ type: DataType.INTEGER, allowNull: true, onDelete: 'NO ACTION' })
  public zn_editor_id: number;

  @BelongsTo(() => Users)
  public editor: Users;

}
