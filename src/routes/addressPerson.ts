import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/addressPerson.controller';

export default [
  {
    path: '/address_person_all/:id',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/address_person/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/address_person',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/address_person/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/address_person/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
