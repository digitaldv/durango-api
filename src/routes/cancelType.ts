import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/cancelType.controller';

export default [
  {
    path: '/cancel_type',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/cancel_type/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/cancel_type',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/cancel_type/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/cancel_type/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
