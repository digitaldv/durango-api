import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet, getStateById, getCitiesFull } from '../controllers/cities.controller';

export default [
  {
    path: '/cities',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/cities/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/cities',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/cities/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/cities/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
  {
    path: '/cities/state/:id',
    method: 'get',
    action: authMiddleware(getStateById),
  },
  {
    path: '/cities/full/:search',
    method: 'get',
    action: getCitiesFull,
  },
];
