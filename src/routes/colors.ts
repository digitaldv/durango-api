import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/colors.controller';

export default [
  {
    path: '/colors',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/colors/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/colors',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/colors/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/colors/:id',
    method: 'delete',
    action: authMiddleware(delet),
  }
];
