import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/countries.controller';

export default [
  {
    path: '/countries',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/countries/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/countries',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/countries/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/countries/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
