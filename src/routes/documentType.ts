import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/documentType.controller';

export default [
  {
    path: '/document_type',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/document_type/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/document_type',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/document_type/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/document_type/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
