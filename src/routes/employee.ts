import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/employee.controller';

export default [
  {
    path: '/employee',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/employee/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/employee',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/employee/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/employee/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];