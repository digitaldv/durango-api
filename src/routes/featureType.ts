import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/featureType.controller';

export default [
  {
    path: '/feature_type',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/feature_type/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/feature_type',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/feature_type/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/feature_type/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
