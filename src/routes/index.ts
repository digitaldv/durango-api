import Users from './users';
import Roles from './roles';
import Cities from './cities';
import DocumentType from './documentType';
import FeatureType from './featureType';
import MotiveType from './motiveType';
import CancelType from './cancelType';
import Marks from './marks';
import Patterns from './patterns';
import Colors from './colors';
import Years from './years';
import Employee from './employee';
import States from './states';
import Countries from './countries';
import Driver from './driver';
import Person from './person';
import RequestDriver from './requestDriver';
import AddressPerson from './addressPerson';
import Pines from './pines';
import Services from './services';
import Passenger from './passenger';
import Questions from './questions';

export default [
  ... Users,
  ... Roles,
  ... Cities,
  ... DocumentType,
  ... FeatureType,
  ... CancelType,
  ... MotiveType,
  ... Marks,
  ... Patterns,
  ... Colors,
  ... Years,
  ... Employee,
  ... States,
  ... Countries,
  ... Driver,
  ... Person,
  ... RequestDriver,
  ... AddressPerson,
  ... Pines,
  ... Services,
  ... Passenger,
  ... Questions,
];
