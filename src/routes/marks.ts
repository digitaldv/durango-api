import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/marks.controller';

export default [
  {
    path: '/marks',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/marks/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/marks',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/marks/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/marks/:id',
    method: 'delete',
    action: authMiddleware(delet),
  }
];
