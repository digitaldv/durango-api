import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/motiveType.controller';

export default [
  {
    path: '/motive_type',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/motive_type/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/motive_type',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/motive_type/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/motive_type/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
