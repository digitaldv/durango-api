import { authMiddleware } from '../functions/authentication';
import { getByUserId } from '../controllers/passenger.controller';

export default [
  {
    path: '/passenger/user/:id',
    method: 'get',
    action: authMiddleware(getByUserId),
  },
];
