import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/patterns.controller';

export default [
  {
    path: '/patterns',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/patterns/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/patterns',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/patterns/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/patterns/:id',
    method: 'delete',
    action: authMiddleware(delet),
  }
];
