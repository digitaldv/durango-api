import { authMiddleware } from '../functions/authentication';
import { getById, update } from '../controllers/person.controller';

export default [
    {
        path: '/person/:id',
        method: 'get',
        action: authMiddleware(getById),
    },
    {
        path: '/person/:id',
        method: 'put',
        action: authMiddleware(update),
    },
];