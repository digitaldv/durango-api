import { authMiddleware } from '../functions/authentication';
import { validatePine, getAllPine } from '../controllers/pines.controller';

export default [
  {
    path: '/pines/validate',
    method: 'post',
    action: authMiddleware(validatePine),
  },
  {
    path: '/pines/:id',
    method: 'get',
    action: authMiddleware(getAllPine),
  },
];
