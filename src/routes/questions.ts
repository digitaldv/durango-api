import { authMiddleware } from '../functions/authentication';
import { getById, createAnswers, getAnswersById } from '../controllers/questions.controller';

export default [
  {
    path: '/questions/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/questions/answers',
    method: 'post',
    action: authMiddleware(createAnswers),
  },
  {
    path: '/questions/answers/:id',
    method: 'get',
    action: authMiddleware(getAnswersById),
  },
];
