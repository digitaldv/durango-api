import { authMiddleware } from '../functions/authentication';
import { getAll, getRequestStatus, getById, update, delet, fileList, fileState } from '../controllers/requestDriver.controller';

export default [
  {
    path: '/request_driver',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/request_driver/status/:id',
    method: 'get',
    action: authMiddleware(getRequestStatus),
  },
  {
    path: '/request_driver/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  // ------------------------------
  {
    path: '/request_driver/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/request_driver/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
  {
    path: '/request_driver/file_list/:id',
    method: 'get',
    action: authMiddleware(fileList),
  },
  {
    path: '/request_driver/file_state/:id',
    method: 'put',
    action: authMiddleware(fileState),
  },
];
