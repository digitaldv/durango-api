import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet, getEmployee } from '../controllers/roles.controller';

export default [
  {
    path: '/roles',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/roles/employee',
    method: 'get',
    action: authMiddleware(getEmployee),
  },
  {
    path: '/roles/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/roles',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/roles/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/roles/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
