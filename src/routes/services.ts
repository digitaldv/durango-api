import { authMiddleware } from '../functions/authentication';
import { getPassengerById, getDriverById } from '../controllers/services.controller';

export default [
  {
    path: '/services/passenger/:id',
    method: 'get',
    action: authMiddleware(getPassengerById),
  },
  {
    path: '/services/driver/:id',
    method: 'get',
    action: authMiddleware(getDriverById),
  },
];
