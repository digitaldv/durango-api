import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/states.controller';

export default [
  {
    path: '/states',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/states/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/states',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/states/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/states/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
];
