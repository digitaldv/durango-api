import {
  authenticateCode, verifyAuthenticate, sendCodeAuth, 
  verifyTokenAuth, authMiddleware,
  authenticateWeb, authenticateToken,
} from '../functions/authentication';
import {
  createPassenger,
  stepOneDriver, stepChangeDriver, stepThreeDriver, createOrUpdateDriverFile, createOrUpdateDriverFileApp, stepStatusDriver,
  getFileById, getFileByStep, getVehicleById, getPersonById, getVehicleByType, getVehicleByPattern
} from '../functions/registration';
import { getAll, getById, create, update, delet, uploadFile, updatePhoto } from '../controllers/user.controller';

export default [
  // --------------------------------------
  {
    path: '/send_code',
    method: 'post',
    action: sendCodeAuth,
  },
  {
    path: '/authenticate_code',
    method: 'post',
    action: authenticateCode, // Se usar en (App/Web [Passenger/Driver])
  },
  {
    path: '/verify_authenticate',
    method: 'post',
    action: verifyAuthenticate,  // Se usar en (App/Web [Passenger/Driver])
  },
  {
    path: '/authenticate/jwt',
    method: 'get',
    action: authenticateToken,  // Se usar en (App/Web [Passenger/Driver/Admin])
  },
  // --------------------------------------
  {
    path: '/verify_token_auth',
    method: 'post',
    action: verifyTokenAuth, // API Spring Boot Java
  },
  {
    path: '/authenticate',
    method: 'post',
    action: authenticateWeb, // Se usar en (Web [Admin])
  },
  // --------------------------------------
  {
    path: '/create_passenger',
    method: 'post',
    action: authMiddleware(createPassenger), // Se usar para el registro (App/Web [Passenger])
  },
  {
    path: '/step_one_driver',
    method: 'post',
    action: authMiddleware(stepOneDriver), // Se usar para el registro (App/Web [Driver])
  },
  {
    path: '/step_three_driver',
    method: 'post',
    action: authMiddleware(stepThreeDriver), // Se usar para el registro (App/Web [Driver])
  },
  {
    path: '/step_file_driver',
    method: 'post',
    action: authMiddleware(createOrUpdateDriverFile), // Se usar para el registro (Web [Driver])
  },
  {
    path: '/step_file_driver_app',
    method: 'post',
    action: authMiddleware(createOrUpdateDriverFileApp), // Se usar para el registro (App [Driver])
  },
  {
    path: '/step_change_driver',
    method: 'post',
    action: authMiddleware(stepChangeDriver), // Se usar para el registro (App/Web [Passenger/Driver])
  },
  {
    path: '/step_status_driver/:id',
    method: 'get',
    action: authMiddleware(stepStatusDriver), // Se usar para el registro (App/Web [Passenger/Driver])
  },
  // --------------------------------------
  {
    path: '/driver_file/:id',
    method: 'get',
    action: authMiddleware(getFileById), // Se usar para el registro (Web [Driver])
  },
  {
    path: '/driver_file_step/:id',
    method: 'get',
    action: authMiddleware(getFileByStep), // Se usar para el registro (App [Driver])
  },
  {
    path: '/driver_vehicle/:id',
    method: 'get',
    action: authMiddleware(getVehicleById), // Se usar para el registro (App/Web [Driver])
  },
  {
    path: '/person_info/:id',
    method: 'get',
    action: authMiddleware(getPersonById), // Se usar para el registro (App/Web [Driver])
  },
  {
    path: '/marks_vehicle/:id',
    method: 'get',
    action: authMiddleware(getVehicleByType), // Se usar para el registro (App/Web [Driver])
  },
  {
    path: '/models_vehicle/:id',
    method: 'get',
    action: authMiddleware(getVehicleByPattern), // Se usar para el registro (App/Web [Driver])
  },
  // --------------------------------------
  {
    path: '/users',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/users/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/users',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/users/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/users/:id',
    method: 'delete',
    action: authMiddleware(delet),
  },
  {
    path: '/users/photo',
    method: 'post',
    action: authMiddleware(updatePhoto),
  },
  {
    path: '/upload_file',
    method: 'post',
    action: authMiddleware(uploadFile),
  },
];
