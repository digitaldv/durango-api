import { authMiddleware } from '../functions/authentication';
import { getAll, getById, create, update, delet } from '../controllers/years.controller';

export default [
  {
    path: '/years',
    method: 'get',
    action: authMiddleware(getAll),
  },
  {
    path: '/years/:id',
    method: 'get',
    action: authMiddleware(getById),
  },
  {
    path: '/years',
    method: 'post',
    action: authMiddleware(create),
  },
  {
    path: '/years/:id',
    method: 'put',
    action: authMiddleware(update),
  },
  {
    path: '/years/:id',
    method: 'delete',
    action: authMiddleware(delet),
  }
];