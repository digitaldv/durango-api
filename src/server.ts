import * as AWS from 'aws-sdk';
import * as dotenv from 'dotenv';
dotenv.config();
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as useragent from 'express-useragent';
// tslint:disable-next-line:no-duplicate-imports
import { Request, Response } from 'express';
import 'reflect-metadata';
import * as errorhandler from 'strong-error-handler';
import routes from './routes';
import sequelizeInstance from './db-config';
// import * as  cluster from 'cluster';
const cluster = require('cluster');
import multer = require('multer');
import os = require('os');

// ACCESS KEY PARA ACCEDER A LOS SERVICIO DE AWS
AWS.config.update({
  // region: 'us-east-1',
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
});

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, 'uploads/') // Directorio donde se guardarán los archivos cargados temporalmente
//   },
//   filename: function (req, file, cb) {
//     cb(null, file.originalname + '-' + Date.now()) // Utiliza el nombre original del archivo
//   }
// });

// const upload = multer({ storage: storage });
const upload = multer({ dest: os.tmpdir() });

sequelizeInstance
  .authenticate()
  .then(() => {
    if (cluster.isMaster) {
      sequelizeInstance.sync({ alter: false, force: false }).then(() => {
        initServer();
      });
    } else {
      initServer();
    }
  })
  .catch(err => {
    console.error(err);
  });

function initServer() {
  const numCPUs = require('os').cpus().length;

  if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    // Fork workers.
    // tslint:disable-next-line:no-increment-decrement
    for (let i = 0; i < 1; i++) {
      cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
      console.log(`worker ${worker.process.pid} died`);
      console.log('worker %d died (%s). restarting...', worker.process.pid, signal || code);
      cluster.fork();
    });
  }
  // tslint:disable-next-line:brace-style
  else {
    const app = express();
    app.use(cors());
    app.use(bodyParser.json({ limit: '10mb' }));
    app.use(useragent.express());
    app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
    app.use(upload.single('photo'));
    // app.use(multer({ storage: storage }).single('file'));
    app.use(
      errorhandler({
        debug: process.env.ENV !== 'prod',
        log: true,
      }),
    );
    routes.forEach(route => {
      app[route.method](
        `/api/v1${route.path}`,
        (request: Request, response: Response, next: (error: Error) => void) => {
          route.action(request, response).then(() => next).catch(err => next(err));
        },
      );
    });
    app.listen(process.env.APP_PORT || 5001);
    console.log(`Worker ${process.pid} started ${process.env.APP_PORT}`);
  }
}
